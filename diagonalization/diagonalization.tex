\documentclass[usenames, dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , angles
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , patterns
  , positioning
  , quotes
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%


\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

% \newcommand{\semitransp}[2][35]{\color{fg!#1}#2}

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\gm}{gm}
\DeclareMathOperator{\am}{am}
\DeclareMathOperator{\trace}{trace}


\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}


\newcommand{\smallsage}[1]{{\small\sage{#1}}}
\newcommand{\footsage}[1]{{\footnotesize\sage{#1}}}
\newcommand{\scriptsage}[1]{{\scriptstyle\sage{#1}}}
\newcommand{\tinysage}[1]{{\tiny\sage{#1}}}


\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Diagonalization}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \tableofcontents[sections={1-3}]
    \column{.5\textwidth}
    \tableofcontents[sections={4-}]
  \end{columns}
\end{frame}




\section{Similar Matrices}
\subsection{Motivation}

\begin{sagesilent}
  P = matrix([(2, 7), (1, 4)])
  D = diagonal_matrix([1, 2])
  A = P*D*P.inverse()
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The factorization
    \[
      \overset{A}{\sage{A}}
      = \overset{P}{\sage{P}}\overset{D}{\sage{D}}\overset{P^{-1}}{\sage{P.inverse()}}
    \]
    helps us compute matrix powers $A^k=PD^kP^{-1}$.
  \end{block}

  \pause
  \begin{block}{Idea}
    The factorization $A=PDP^{-1}$ allows us to study the ``easy'' $D$ instead
    of the ``hard'' $A$.
  \end{block}

  \pause
  \begin{block}{Observation}
    The diagonal entries of $D$ are eigenvalues of $A$ and the columns of $P$
    are corresponding eigenvectors.
  \end{block}

\end{frame}


\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We say that $A$ is \emph{similar} to $B$ if $A=PBP^{-1}$.
  \end{definition}

  \pause
  \begin{block}{Notation}
    We often write $A\sim B$ to indicate that $A$ is similar to $B$.
  \end{block}

  \pause
  \begin{example}
    Our previous factorization
    \[
      \overset{A}{\sage{A}}
      = \overset{P}{\sage{P}}\overset{D}{\sage{D}}\overset{P^{-1}}{\sage{P.inverse()}}
    \]
    indicates that $A\sim D$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{theorem}
    Similar matrices share many important structural properties.
    \begin{description}[Other description]
    \item<+->[rank] $\rank(A)=\rank(B)$
    \item<+->[char-poly] $\chi_A(t)=\chi_B(t)$
    \item<+->[trace] $\trace(A)=\trace(B)$
    \item<+->[determinant] $\det(A)=\det(B)$
    \item<+->[eigenvalues] $\operatorname{E-Vals}(A)=\operatorname{E-Vals}(B)$
    \item<+->[alg-mults] $\am_A(\lambda)=\am_B(\lambda)$
    \item<+->[geo-mults] $\gm_A(\lambda)=\gm_B(\lambda)$
    \end{description}
  \end{theorem}

\end{frame}


\begin{sagesilent}
  set_random_seed(9842)
  l1, l2 = 0, -3
  J1 = jordan_block(l1, 2)
  J2 = jordan_block(l2, 1)
  B = block_diagonal_matrix(J1, J2, subdivide=False)
  P = random_matrix(ZZ, B.nrows(), algorithm='unimodular', upper_bound=10)
  A = P*B*P.inverse()
  var('t')
  chi = factor(B.characteristic_polynomial(t))
  I = identity_matrix(A.nrows())
  EA1 = Set((A-l1*I).change_ring(ZZ).right_kernel().basis())
  EA2 = Set((A-l2*I).change_ring(ZZ).right_kernel().basis())
  EB1 = Set((B-l1*I).change_ring(ZZ).right_kernel().basis())
  EB2 = Set((B-l2*I).change_ring(ZZ).right_kernel().basis())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the factorization
  \[
    \overset{A}{\footsage{A}}
    =
    \overset{P}{\footsage{P}}
    \overset{B}{\footsage{B}}
    \overset{P^{-1}}{\footsage{P.inverse()}}
  \]\pause
  This means that $A\sim B$. \pause The characteristic polynomial is
  \[
    \chi_A(t)=\chi_B(t)=\sage{chi}
  \]\pause
  The eigenspaces are given by
  \vspace{1em}
  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \centering
    \myotherbold{Eigenspaces of $A$}
    \setlength{\abovedisplayskip}{2pt}
    \begin{align*}
      E_{\sage{l1}} &= \Span\sage{EA1} \\
      E_{\sage{l2}} &= \Span\sage{EA2}
    \end{align*}
    \column{.5\textwidth}
    \centering
    \myotherbold{Eigenspaces of $B$}
    \setlength{\abovedisplayskip}{2pt}
    \begin{align*}
      E_{\sage{l1}} &= \Span\sage{EB1} \\
      E_{\sage{l2}} &= \Span\sage{EB2}
    \end{align*}
  \end{columns}
  \vspace{1em}\pause
  The eigenspaces are different!

\end{frame}


\section{Diagonalizable Matrices}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We say that $A$ is \emph{diagonalizable} if $A$ is similar to a diagonal
    matrix.
  \end{definition}

  \pause
  \begin{block}{Idea}
    Diagonalizable means $A=PDP^{-1}$ where $D$ is diagonal. Rather than study
    the ``hard'' $A$, we can study the ``easy'' $D$.
  \end{block}

  \pause
  \begin{block}{Note}
    Strang writes $A=X\Lambda X^{-1}$.
  \end{block}

\end{frame}



\begin{sagesilent}
  set_random_seed(41801)
  l1, l2 = -7, 4
  D = diagonal_matrix([l1, l1, l2])
  P = random_matrix(ZZ, D.nrows(), algorithm='unimodular', upper_bound=5)
  A = P*D*P.inverse()
  v1, v2, v3 = P.columns()
  E1 = Set((v1, v2))
  E2 = Set((v3,))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myM}[3]{
    \begin{bmatrix}
      ####1 & ####2 & ####3
    \end{bmatrix}
  }
  Consider the factorization
  \[
    \overset{A}{\sage{A}}
    =
    \overset{P}{\footsage{P}}
    \overset{D}{\footsage{D}}
    \overset{P^{-1}}{\footsage{P.inverse()}}
  \]\pause
  This is equivalent to $AP=PD$. \pause Writing
  $P=\myM{\vv*{v}{1}}{\vv*{v}{2}}{\vv*{v}{3}}$ gives
  \[
    \overset{\myotherbold{AP}}{\myM{A\vv*{v}{1}}{A\vv*{v}{2}}{A\vv*{v}{3}}}
    =
    \overset{\myotherbold{PD}}{\myM{\sage{l1}\cdot\vv*{v}{1}}{\sage{l1}\cdot\vv*{v}{2}}{\sage{l2}\cdot\vv*{v}{3}}}
  \]\pause
  Evidently, the eigenvalues of $A$ are $\lambda_1=\pause\sage{l1}$ \pause and
  $\lambda_2=\pause \sage{l2}$. \pause

  The eigenspaces are
  \begin{align*}
    E_{\sage{l1}} &= \Span\sage{E1} & E_{\sage{l2}} &= \Span\sage{E2}
  \end{align*}

\end{frame}



\begin{sagesilent}
  var('t')
  A = jordan_block(0, 2)
  chi = factor(A.characteristic_polynomial(t))
  D = diagonal_matrix([0, 0])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the matrix $A$ given by
  \[
    A = \sage{A}
  \]\pause
  The characteristic polynomial of $A$ is $\chi_A(t)=\pause\sage{chi}$. \pause
  If $A$ were diagonalizable, the only suitable choice for $D$ would be
  \[
    D = \pause\sage{D}
  \]\pause
  But then $A=PDP^{-1}=\pause O_{2\times 2}\pause\neq A$. \pause The matrix $A$
  is \emph{not diagonalizable}!

\end{frame}



\subsection{Testing for Diagonalizability}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we determine if a matrix is diagonalizable?
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Test for Diagonalizability]
    An $n\times n$ matrix $A$ is diagonalizable if and only if
    \[
      \gm_A(\lambda_1)+\gm_A(\lambda_2)+\dotsb+\gm_A(\lambda_k)=n
    \]
    where $\Set{\lambda_1, \lambda_2, \dotsc, \lambda_k}$ are the eigenvalues of
    $A$.
  \end{theorem}

  \pause
  \begin{theorem}[Another Test for Diagonalizability]
    A matrix $A$ is diagonalizable if and only if each eigenvalue $\lambda$
    satisfies $\gm_A(\lambda)=\am_A(\lambda)$.
  \end{theorem}

\end{frame}



\begin{sagesilent}
  set_random_seed(7942)
  l1, l2 = -1, 2
  A = random_matrix(ZZ, 3, algorithm='diagonalizable', eigenvalues=(l1, l2), dimensions=(1, 2))
  n = A.nrows()
  I = identity_matrix(n)
  evals = Set([l1, l2])
  A1 = A-l1*I
  A2 = A-l2*I
  g1 = A1.right_nullity()
  g2 = A2.right_nullity()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the matrix $A$ given by
  \begin{align*}
    A &= \tinysage{A} & \onslide<2->{\operatorname{E-Vals}(A) &= \sage{evals}}
  \end{align*}
  \onslide<3->{The geometric multiplicities of the eigenvalues are}
  \begin{align*}
    \onslide<4->{\gm_A(\sage{l1}) &= \dim(E_{\sage{l1}})}                                    & \onslide<8->{\gm_A(\sage{l2}) &= \dim(E_{\sage{l2}})}                     \\
                                  &\onslide<5->{= \nullity(A-(\sage{l1})\cdot I_{\sage{n}})} &                               &\onslide<9->{=\nullity(A-\sage{l2}\cdot I_{\sage{n}})} \\
                                  &\onslide<6->{= \nullity\tinysage{A1}}                     &                               &\onslide<10->{=\nullity\tinysage{A2}}                   \\
                                  &\onslide<7->{= \sage{g1}}                                 &                               &\onslide<11->{=\sage{g2}}
  \end{align*}
  \onslide<12->{Summing the geometric multiplicities gives}
  \[
    \onslide<13->{\gm_A(\sage{l1})+\gm_A(\sage{l2})=\sage{g1}+\sage{g2}=\sage{g1+g2}}
  \]
  \onslide<14->{the matrix $A$ \emph{is diagonalizable}.}
\end{frame}



\begin{sagesilent}
  set_random_seed(4279)
  l1, l2 = -3, 4
  J1 = jordan_block(l1, 2)
  J2 = jordan_block(l2, 1)
  J = block_diagonal_matrix(J1, J2, J2)
  n = J.nrows()
  P = random_matrix(ZZ, n, algorithm='unimodular', upper_bound=4)
  A = P*J*P.inverse()
  I = identity_matrix(n)
  evals = Set([l1, l2])
  var('t')
  chi = factor(A.characteristic_polynomial(t))
  A1 = A-l1*I
  g1 = A1.right_nullity()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the matrix $A$ given by
  \begin{align*}
    A &= \tinysage{A} & \chi_A(t) &= \sage{chi}
  \end{align*}\pause
  Note that
  \[
    \gm_A(\sage{l1})
    = \pause\nullity\overset{A-(\sage{l1})\cdot I_{\sage{n}}}{\tinysage{A1}}
    = \pause\nullity\overset{\rref(A-(\sage{l1})\cdot I_{\sage{n}})}{\tinysage{A1.rref()}}
    = \pause\sage{g1}
  \]\pause
  Since $\gm_A(\sage{l1})=\sage{g1}<2=\am_A(\sage{l1})$, the matrix $A$ is
  \emph{not diagonalizable}.

\end{frame}




\section{The Diagonalization Algorithm}
\subsection{Statement}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Suppose that $A$ is diagonalizable. How do we find $A=PDP^{-1}$?
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{algorithm}[Diagonalizing a Matrix]
    Let $A$ be an $n\times n$ matrix. The following steps will produce
    $A=PDP^{-1}$ or determine that $A$ is diagonalizable.
    \begin{description}
    \item<2->[Step 1] Find the eigenvalues $\Set{\lambda_1,\dotsc,\lambda_k}$ of $A$.
    \item<3->[Step 2] Test if $A$ is diagonalizable.
      \begin{description}
      \item<4->[Test 1] $\gm_A(\lambda_j)=\am_A(\lambda_j)$
      \item<5->[Test 2] $\Sigma \gm_A(\lambda_j)=n$
      \end{description}
    \item<6->[Step 3] Consider empty $n\times n$ matrices $P$ and $D$.
    \item<7->[Step 4] For each eigenvalue $\lambda$:
      \begin{description}
      \item<8->[Find a basis] $E_\lambda=\Span\Set{\vv*{v}{1},\dotsc\vv*{v}{d}}$
      \item<9->[Alter $P$] Put $\Set{\vv*{v}{1},\dotsc\vv*{v}{d}}$ into the columns.
      \item<10->[Alter $D$] Put $\lambda$ in the diagonal $d$-times.
      \end{description}
    \end{description}
    \onslide<11->{This produces $A=PDP^{-1}$.}
  \end{algorithm}

\end{frame}


\subsection{Examples}
\begin{sagesilent}
  set_random_seed(2479)
  l1, l2 = -7, 18
  n = 3
  A = random_matrix(ZZ, n, algorithm='diagonalizable', eigenvalues=(l1, l2), dimensions=(2, 1))
  I = identity_matrix(n)
  v1, v2 = (A-l1*I).change_ring(QQ).right_kernel(basis='pivot').basis()
  v3, = (A-l2*I).change_ring(QQ).right_kernel(basis='pivot').basis()
  var('t')
  chi = factor(A.characteristic_polynomial(t))
  E1 = Set((v1, v2))
  E2 = Set((v3,))
  P = matrix.column([v1, v2, v3])
  D = diagonal_matrix([l1, l1, l2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the matrix $A$ given by
  \begin{align*}
    A &= \tinysage{A} & \chi_A(t) &= \sage{chi}
  \end{align*}\pause
  Bases for the eigenspaces are given by
  \begin{align*}
    E_{\sage{l1}} &= \Span\sage{E1} & E_{\sage{l2}} &= \Span\sage{E2}
  \end{align*}\pause
  This gives the diagonalization $A=PDP^{-1}$ where
  \begin{align*}
    P &= \sage{P} & D &= \sage{D}
  \end{align*}

\end{frame}



\begin{sagesilent}
  P1 = matrix.column([v2, v1, v3])
  D1 = diagonal_matrix([l1, l1, l2])
  P2 = matrix.column([v3, v1, v2])
  D2 = diagonal_matrix([l2, l1, l1])
  P3 = matrix.column([v1, v3, v2])
  D3 = diagonal_matrix([l1, l2, l1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The previous example gave $A=PDP^{-1}$ where
    \begin{align*}
      P &= \footsage{P} & D &= \footsage{D}
    \end{align*}
    \onslide<2->{Other suitable choices for $A=PDP^{-1}$ are}
    \begin{align*}
      \onslide<3->{P &= \footsage{P1} & D &= \footsage{D1}} \\
      \onslide<4->{P &= \footsage{P2} & D &= \footsage{D2}} \\
      \onslide<5->{P &= \footsage{P3} & D &= \footsage{D3}}
    \end{align*}
  \end{block}

\end{frame}




\begin{sagesilent}
  var('a')
  l = 3
  J1 = matrix(2, map(lambda x: x if x != 1 else a, jordan_block(l, 2).list()))
  J2 = jordan_block(l, 1)
  A = block_diagonal_matrix(J1, J2, subdivide=False)
  var('t')
  chi = factor(A.characteristic_polynomial(t).change_ring(ZZ))
  n = A.nrows()
  I = identity_matrix(n)
  A1 = A-l*I
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    For which values of $a$ is $A = \sage{A}$ diagonalizable?
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that $\chi_A(t)=\pause\sage{chi}$. \pause The only eigenvalue is
    $\lambda=\pause\sage{l}$. \pause The geometric multiplicity is
    \[
      \gm_A(\sage{l})
      = \pause\nullity\sage{A1}
      = \pause
      \begin{cases}
        2 & a\neq 0 \\
        3 & a=0
      \end{cases}
    \]\pause
    The only value of $a$ for which $A$ is diagonalizable is $a=0$.
  \end{block}

\end{frame}



\begin{sagesilent}
  A = matrix(SR, [(0, 1), (-1, 0)])
  J, P = A.jordan_form(transformation=True)
  evals = J.diagonal()
  D = diagonal_matrix(evals)
  l1, l2 = evals
  v1, v2 = P.columns()
  E1 = Set((v1,))
  E2 = Set((v2,))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  We previously found that the eigenspaces of
  \[
    A = \sage{A}
  \]
  are given by
  \begin{align*}
    E_{\sage{l1}} &= \Span\sage{E1} & E_{\sage{l2}} &= \Span\sage{E2}
  \end{align*}\pause
  This gives the diagonalization $A=PDP^{-1}$ where
  \begin{align*}
    P &= \sage{P} & D &= \sage{D}
  \end{align*}

\end{frame}




\section{The Fibonacci Numbers}
\subsection{Definition}

\begin{sagesilent}
  F = fibonacci_sequence(10)
  f0, f1, f2, f3, f4, f5, f6, f7, f8, f9 = F
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{Fibonacci sequence} $\Set{F_k}$ is recursively defined by
    \begin{align*}
      \onslide<2->{F_0 &= 0} & \onslide<3->{F_1 &= 1} & \onslide<4->{F_{k+2} &= F_{k+1}+F_k}
    \end{align*}
    \onslide<5->{Evidently,}
    $
    \onslide<6->{\Set{F_k}
      =
      \{
    }
    \onslide<7->{\sage{f0},}
    \onslide<8->{\sage{f1},}
    \onslide<9->{\sage{f2},}
    \onslide<10->{\sage{f3},}
    \onslide<11->{\sage{f4},}
    \onslide<12->{\sage{f5},}
    \onslide<13->{\sage{f6},}
    \onslide<14->{\sage{f7},}
    \onslide<15->{\sage{f8},}
    \onslide<16->{\sage{f9},}
    \onslide<17->{\dotsc}
    \onslide<18->{\}}
    $\onslide<19->{.}
  \end{definition}

  \onslide<20->
  \begin{block}{Question}
    Can we efficiently compute $F_{100}$?
  \end{block}

  \onslide<21->
  \begin{block}{Idea}
    Find a formula for $F_k$.
  \end{block}

\end{frame}




\subsection{Difference Equation}
\newcommand{\myU}[2]{
  \left[
    \begin{array}{c}
      F_{#1}\\ F_{#2}
    \end{array}
  \right]
}
\newcommand{\myV}[2]{
  \left[
    \begin{array}{c}
      #1\\ #2
    \end{array}
  \right]
}
\begin{sagesilent}
  u = lambda k: matrix.column([fibonacci(k+1), fibonacci(k)])
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Define $\vv*{u}{k}=\myU{k+1}{k}$. \onslide<2->{Then}
  \begin{align*}
    \onslide<3->{\vv*{u}{0} &= \myU{1}{0}}               & \onslide<5->{\vv*{u}{1} &= \myU{2}{1}}               & \onslide<7->{\vv*{u}{2} &= \myU{3}{2}}               & \onslide<9->{\vv*{u}{3} &= \myU{4}{3}} \\
                            &\onslide<4->{= \sage{u(0)}} &                         &\onslide<6->{= \sage{u(1)}} &                         &\onslide<8->{= \sage{u(2)}} &                         &\onslide<10->{= \sage{u(3)}}
  \end{align*}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Since $F_{k+2}=F_{k+1}+F_k$ and $\vv*{u}{k}=\myU{k+1}{k}$, we have
    \newcommand{\myA}{
      \left[
        \begin{array}{rr}
          1 & 1 \\
          1 & 0
        \end{array}
      \right]
    }
    \[
      \vv*{u}{k+1}
      = \pause\myU{k+2}{k+1}
      = \pause\myV{F_{k+1}+F_k}{F_k}
      = \pause\overset{A}{\myA}\overset{\vv*{u}{k}}{\myU{k+1}{k}}
    \]\pause
    This gives the equation $\vv*{u}{k+1}=A\vv*{u}{k}$.
  \end{block}

\end{frame}



\begin{sagesilent}
  A = matrix([(1, 1), (1, 0)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The equation $\vv*{u}{k+1}=A\vv*{u}{k}$ gives
    \begin{align*}
      \onslide<2->{\vv*{u}{1} &= A\vv*{u}{0}} & \onslide<3->{\vv*{u}{2} &= A\vv*{u}{1}}                & \onslide<5->{\vv*{u}{3} &= A\vv*{u}{2}}                & \onslide<7->{\vv*{u}{4} &= A\vv*{u}{3}} \\
                              &               &                         &\onslide<4->{= A^2\vv*{u}{0}} &                         &\onslide<6->{= A^3\vv*{u}{0}} &                         &\onslide<8->{= A^4\vv*{u}{0}}
    \end{align*}
    \onslide<9->{Continuing with this pattern gives $\vv*{u}{k}=\onslide<10->{A^k\vv*{u}{0}$.}}
  \end{block}

  \onslide<11->
  \begin{block}{Observation}
    Finding a formula for $A^k$ gives a formula for $\vv*{u}{k}$ and hence
    $F_k$. We need to diagonalize $A$!
  \end{block}

\end{frame}


\subsection{Diagonalizing}

\begin{sagesilent}
  var('t')
  chi = A.characteristic_polynomial(t)
  I = identity_matrix(A.nrows())
  A1 = t*I-A
  c, b, a = chi.coefficients()
\end{sagesilent}
\begin{frame}[fragile]

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Compute the Eigenvalues}
    The characteristic polynomial of $A=\sage{A}$ is
    \begin{sagesilent}latex.matrix_delimiters(left='|', right='|')\end{sagesilent}
    \[
      \chi_A(t)
      = \pause\sage{A1}
      = \pause\sage{chi}
    \]\pause
    The roots of $\chi_A(t)$ are given by
    \[
      \oldfrac{
        -(\sage{b})\pm\sqrt{(\sage{b})^2-4(\sage{a})(\sage{c})}
      }{
        2(\sage{a})
      }
      = \pause\oldfrac{
        1\pm\sqrt{5}
      }{
        2
      }
    \]\pause
    The eigenvalues of $A$ are thus
    \begin{align*}
      \lambda_1 &= \frac{1+\sqrt{5}}{2} & \lambda_2 &= \frac{1-\sqrt{5}}{2}
    \end{align*}\pause
    Both eigenvalues satisfy $\lambda_j^2-\lambda_j-1=0$.
  \end{block}
  \begin{sagesilent}latex.matrix_delimiters(left='[', right=']')\end{sagesilent}

\end{frame}



\begin{sagesilent}
  var('l', latex_name='\lambda')
  A1 = l*I-A
  from functools import partial
  elem = partial(elementary_matrix, A.nrows())
  E1 = elem(row1=0, row2=1, scale=l-1)
  A2 = expand(E1*A1)
  A3 = copy(A2)
  A3[0, 1] = 0
  v = matrix.column([l, 1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Bases of the Eigenspaces}
    Each eigenvalue of $A$ satisfies $\lambda^2-\lambda-1=0$. \onslide<2->{This gives}
    \begin{align*}
      \onslide<3->{E_\lambda
      &=} \onslide<4->{\Null\sage{A1} \\
      &=} \onslide<5->{\Null\sage{A2} \\
      &=} \onslide<6->{\Null\sage{A3} \\
      &=} \onslide<7->{\Span\Set*{\sage{v}}}
    \end{align*}
    \onslide<8->{Our two eigenspaces are then given by}
    \begin{align*}
      \onslide<9->{E_{\lambda_1} &= \Span\Set{\langle \lambda_1, 1\rangle} & E_{\lambda_2} &= \Span\Set{\langle \lambda_2, 1\rangle}}
    \end{align*}
  \end{block}


\end{frame}



\begin{sagesilent}
  var('l1', latex_name='\lambda_1')
  var('l2', latex_name='\lambda_2')
  var('k')
  v1 = vector([l1, 1])
  v2 = vector([l2, 1])
  P = matrix.column([v1, v2])
  D = diagonal_matrix([l1, l2])
  Dk = diagonal_matrix([l1**k, l2**k])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The $A=PDP^{-1}$ Factorization}
    The bases for our eigenspaces are
    \begin{align*}
      E_{\lambda_1} &= \Span\Set{\langle \lambda_1, 1\rangle} & E_{\lambda_2} &= \Span\Set{\langle \lambda_2, 1\rangle}
    \end{align*}\pause
    This gives the diagonalization $A=PDP^{-1}$ where
    \begin{align*}
      P &= \sage{P} & D &= \sage{D} & P^{-1} &= \sage{1/P.det()}\sage{P.adjoint()}
    \end{align*}
  \end{block}

\end{frame}



\subsection{The General Formula}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Now, we may compute $\vv*{u}{k}=A^k\vv*{u}{0}$. \onslide<2->{This gives}
  \begin{align*}
    \onslide<3->{\vv*{u}{k}
    &=} \onslide<4->{A^k\vv*{u}{0} \\
    &=} \onslide<5->{PD^kP^{-1}\vv*{u}{0} \\
    &=} \onslide<6->{\overset{P}{\sage{P}}\overset{D^k}{{\sage{D}}^k}
      \overset{P^{-1}}{
      \left(
      \sage{1/P.det()}
      \sage{P.adjoint()}
      \right)
      }
      \sage{u(0)} \\
    &=} \onslide<7->{\sage{1/P.det()}\sage{P}\sage{Dk}\sage{P.adjoint()*u(0)} \\
    &=} \onslide<8->{\frac{1}{\sqrt{5}}\sage{P}\sage{Dk*P.adjoint()*u(0)} \\
    &=} \onslide<9->{\frac{1}{\sqrt{5}}\sage{P*Dk*P.adjoint()*u(0)}}
  \end{align*}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Since $\vv*{u}{k}=\myU{k+1}{k}$, the equation
  \[
    \vv*{u}{k}=\frac{1}{\sqrt{5}}\sage{P*Dk*P.adjoint()*u(0)}
  \]
  gives our desired general formula
  \[
    F_k=\pause\frac{\lambda_1^k-\lambda_2^k}{\sqrt{5}}
  \]\pause
  We can then compute $F_{100}$ with
  \[
    F_{100}
    = \pause\frac{\lambda_1^{100}-\lambda_2^{100}}{\sqrt{5}}
    = \pause\sage{fibonacci(100)}
  \]

\end{frame}



\subsection{The Golden Ratio}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The number $\phi=\displaystyle\frac{1+\sqrt{5}}{2}$ is called the
    \emph{golden ratio}.\pause
    \[
      \begin{tikzpicture}[line join=round, line cap=round]
        \coordinate (start) at (0, 0);
        \coordinate (end) at (9, 0);

        \pgfmathsetmacro{\myPhi}{2/(1+sqrt(5))}
        \coordinate (a) at ($ \myPhi*(end) $);

        \coordinate (tick) at (0, 3pt);

        \draw[ultra thick] (start) -- (a) node [midway, above] {$a$};
        \draw[ultra thick] (a) -- (end) node [midway, above] {$b$};

        \draw[ultra thick] ($ (start)+(tick) $) -- ($ (start)-(tick) $);
        \draw[ultra thick] ($ (end)+(tick) $) -- ($ (end)-(tick) $);
        \draw[ultra thick] ($ (a)+(tick) $) -- ($ (a)-(tick) $);
      \end{tikzpicture}
    \]\pause
    This figure is considered aesthetically pleasing when
    \[
      \frac{a}{b}=\frac{a+b}{a}
    \]\pause
    By defining $\phi=\frac{a}{b}$, we obtain \pause
    \[
      \phi
      = \pause\frac{a}{b}
      = \pause\frac{a+b}{a}
      = \pause1+\frac{b}{a}
      = \pause1+\frac{1}{\phi}
    \]\pause
    Then $\phi^2=\pause\phi+1$ \pause so that $\phi^2-\phi-1=\pause 0$.
  \end{block}

\end{frame}



\end{document}
