\documentclass[usenames, dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\DeclareSIUnit{\mph}{mph}
\DeclareSIUnit\salt{salt}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , angles
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , patterns
  , positioning
  , quotes
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%


\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

% \newcommand{\semitransp}[2][35]{\color{fg!#1}#2}

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\gm}{gm}
\DeclareMathOperator{\am}{am}
\DeclareMathOperator{\trace}{trace}
\DeclareMathOperator{\diag}{diag}


\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}


\newcommand{\smallsage}[1]{{\small\sage{#1}}}
\newcommand{\footsage}[1]{{\footnotesize\sage{#1}}}
\newcommand{\scriptsage}[1]{{\scriptstyle\sage{#1}}}
\newcommand{\tinysage}[1]{{\tiny\sage{#1}}}


\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Systems of Linear Ordinary Differential Equations}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-3}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={4-}]
  % \end{columns}
\end{frame}




\newcommand{\myTank}[3]{
  \begin{array}{c}
    \begin{tikzpicture}[line join=round, line cap=round]

      \node[
      , cylinder
      , ultra thick
      , shape border rotate=90
      , draw
      , aspect=.4
      , minimum height=#2cm
      , minimum width=#3cm
      , cylinder uses custom fill
      , cylinder body fill=blue!30
      , cylinder end fill=blue!10
      ]
      (A) {\SI{#1}{\liter}};

    \end{tikzpicture}
  \end{array}
}

\newcommand{\mySystem}{
  \begin{tikzcd}[column sep=large]
    \arrow[blue, rightsquigarrow]{r}[black]{\SI[per-mode=repeated-symbol]{10}{\liter\per\second}}
    \pgfmatrixnextcell\myTank{20}{1}{2}
    \arrow[blue, rightsquigarrow]{r}[black]{\SI[per-mode=repeated-symbol]{10}{\liter\per\second}}
    \pgfmatrixnextcell\myTank{40}{2}{2}
    \arrow[blue, rightsquigarrow]{r}[black]{\SI[per-mode=repeated-symbol]{10}{\liter\per\second}}
    \pgfmatrixnextcell\phantom{j}
  \end{tikzcd}
}

\section{Motivation}
\subsection{The ``Brine Tank'' Problem}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider a cascade of salt water tanks.
  \[
    \mySystem
  \]
  \onslide<+->\onslide<+-> Initially, the salt content is \SI{10}{\kg} in Tank 1
  and \SI{30}{\kg} in Tank 2.
  \begin{enumerate}[<+->]
  \item Pure water is pumped into Tank 1 at a rate of
    \SI[per-mode=repeated-symbol]{10}{\liter\per\second}.
  \item The salt water in Tank 1 is mixed uniformly and pumped into Tank 2 at a
    rate of \SI[per-mode=repeated-symbol]{10}{\liter\per\second}.
  \item The salt water in Tank 2 is mixed uniformly and pumped out at a rate of
    \SI[per-mode=repeated-symbol]{10}{\liter\per\second}.
  \end{enumerate}
  \onslide<+-> How much salt is in each tank at time $t$?
\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \mybold{Chemical Balance Law} $\textnormal{rate of change}=\textnormal{input rate}-\textnormal{output rate}$\onslide<+->\onslide<+->
  \[
    \mySystem
  \]
  \onslide<+-> Let $u_i(t)$ be the amount of salt in the $i$th tank at time
  $t$.\onslide<+-> \newcommand{\mySI}[2]{\SI[per-mode=repeated-symbol,
    parse-numbers=false, number-math-rm=\ensuremath]{####1}{####2}}
  \[
    \begin{array}{rclclcl}
      u_1^\prime &=& \oldfrac{\SI{0}{\kg\salt}}{\SI{20}{\liter}}\cdot\mySI{10}{\liter\per\second} &-& \oldfrac{\mySI{u_1}{\kg\salt}}{\SI{20}{\liter}}\cdot\mySI{10}{\liter\per\second} &=& -\oldfrac{1}{2}u_1 \\ \\
      \onslide<+->u_2^\prime &=& \oldfrac{\mySI{u_1}{\kg\salt}}{\SI{20}{\liter}}\cdot\mySI{10}{\liter\per\second} &-& \oldfrac{\mySI{u_2}{\kg\salt}}{\SI{40}{\liter}}\cdot\mySI{10}{\liter\per\second} &=& \phantom{-}\oldfrac{1}{2}u_1-\oldfrac{1}{4}u_2
    \end{array}
  \]
  \onslide<+-> This is a \emph{linear system of ordinary differential equations}.
\end{frame}


\begin{sagesilent}
  A = matrix([(-1/2, 0), (1/2, -1/4)])
  var('u1 u2')
  u0 = matrix.column([10, 30])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Our system is of the form
    \[\def\arraystretch{1.2}
      \begin{array}{rcrcrcrcl}
        u_1^\prime &=& -\oldfrac{1}{2}\,u_1 &&&& u_1(0) &=& 10 \\
        u_2^\prime &=& \oldfrac{1}{2}\,u_1 &-& \oldfrac{1}{4}\,u_2 && u_2(0) &=& 30 \\
      \end{array}
    \]
    \pause Defining $\vv{u}(t)=\langle u_1(t), u_2(t)\rangle$ allows us to write
    \newcommand{\myV}[2]{ \left[
        \begin{array}{c}
          ####1\\ ####2
        \end{array}
      \right]
    }
    \begin{align*}
      \overset{\vv{u}^\prime}{\myV{u_1^\prime}{u_2^\prime}} &= \overset{A}{\sage{A}} \overset{\vv{u}}{\myV{u_1}{u_2}} & \overset{\vv{u}(0)}{\myV{u_1(0)}{u_2(0)}} &= \overset{\vv*{u}{0}}{\myV{10}{30}}
    \end{align*}
    \pause How do we solve this?
  \end{block}

\end{frame}



\section{Matrix Exponentials}
\subsection{An Idea from Calculus}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do we solve $\vv{u}^\prime=A\vv{u}$ with initial condition
    $\vv{u}(0)=\vv*{u}{0}$?
  \end{block}

  \pause
  \begin{block}{Recall}
    The \emph{initial value problem}
    \begin{align*}
      y^\prime &= ky & y(0) &= y_0
    \end{align*}
    is uniquely solved by $y(t)=\pause y_0 e^{kt}$.
  \end{block}


  \pause
  \begin{block}{Idea}
    If exponentials are used to solve $y^\prime=ky$, maybe they can also be used
    to solve $\vv{u}^\prime=A\vv{u}$.
  \end{block}

\end{frame}



\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The exponential function $e^t=\exp(t)$ may be written as
    \[
      \exp(t)
      =
      \sum_{k=0}^\infty\frac{1}{k!}t^k
    \]
    \pause This is the \emph{Taylor series representation} of $\exp(t)$.
  \end{block}

  \pause
  \begin{definition}
    The \emph{exponential} of a square matrix $A$ is
    \[
      \exp(A)
      = e^A
      = \sum_{k=0}^\infty\frac{1}{k!}A^k
    \]
    \pause The exponential $\exp(A)$ is a square matrix with the same size as
    $A$.
  \end{definition}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Matrix exponentials satisfy the following properties.
    \begin{enumerate}[<+->]
    \item $\exp(O_{n\times n})=I_n$
    \item $\exp(A^\intercal)=\exp(A)^\intercal$
    \item $\exp(PAP^{-1})=P\exp(A)P^{-1}$
    \item $AB=BA\implies e^Ae^B=e^{A+B}$
    \item $\exp(A)^{-1}=\exp(-A)$
    \item $\det(e^A)=e^{\trace(A)}$
    \item $e^{\diag(d_1,\dotsc,d_n)}=\diag(e^{d_1},\dotsc, e^{d_n})$
    \end{enumerate}
  \end{theorem}

\end{frame}


\subsection{Example}

\begin{sagesilent}
  P = matrix([(-2, -3), (1, 1)])
  D = diagonal_matrix([-7, 2])
  A = P*D*P.inverse()
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Compute $\exp(A)$ where $A=\sage{A}$.

  \onslide<2->
  \begin{block}{Solution}
    Start by diagonalizing $A$.
    \[
      \overset{A}{\sage{A}}
      =
      \overset{P}{\sage{P}}
      \overset{D}{\sage{D}}
      \overset{P^{-1}}{\sage{P.inverse()}}
    \]
    \onslide<3-> The matrix exponential $\exp(A)$ is then
    \newcommand{\myD}[2]{
      \left[
        \begin{array}{cc}
          ####1 & 0 \\
          0     & ####2
        \end{array}
      \right]
    }
    \newcommand{\myE}{
      \left[
        \begin{array}{rr}
          3\,e^2-2\,e^{-7} & 6\,e^2-6\,e^{-7} \\
          -e^2+e^{-7}      & -2\,e^2+3\,e^{-7}
        \end{array}
      \right]
    }
    \begin{align*}
      \onslide<4->{\exp(A) &= \overset{P}{\sage{P}} \overset{\exp(D)}{\myD{e^{-7}}{e^{2}}} \overset{P^{-1}}{\sage{P.inverse()}}} \\
                           &\onslide<5->{= \myE}
    \end{align*}

  \end{block}

\end{frame}


\section{Solving Systems of ODEs}
\subsection{Formula Using Exponentials}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The initial value problem
    \begin{align*}
      \vv{u}^\prime(t) &= A\vv{u} & \vv{u}(0) &= \vv*{u}{0}
    \end{align*}
    has a unique solution given by $\vv{u}(t)=\exp(At)\vv*{u}{0}$.
  \end{theorem}

  \pause
  \begin{block}{Note}
    For $A=PDP^{-1}$, we have $\vv{u}(t)=P\exp(Dt)P^{-1}\vv*{u}{0}$.
  \end{block}

\end{frame}



\subsection{Example}

\begin{sagesilent}
  u0 = matrix.column([-3, 1])
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the initial value problem defined by the data
  \begin{align*}
    A &= \sage{A} & \vv*{u}{0} &= \sage{u0}
  \end{align*}
  \pause We previously factored $A$ as
  \[
    \overset{A}{\sage{A}}
    =
    \overset{P}{\sage{P}}
    \overset{D}{\sage{D}}
    \overset{P^{-1}}{\sage{P.inverse()}}
  \]
  \pause The solution to our initial value problem is
  \newcommand{\myDt}{
    \left[
      \begin{array}{rr}
        e^{-7\,t} & 0 \\
        0         & e^{2\,t}
      \end{array}
    \right]
  }
  \newcommand{\myU}{
    \left[
      \begin{array}{c}
        -3\,e^{2\,t}\\ e^{2\,t}
      \end{array}
    \right]
  }
  \[
    \vv{u}(t)
    = \overset{P}{\sage{P}}\overset{\exp(Dt)}{\myDt}\overset{P^{-1}}{\sage{P.inverse()}}\overset{\vv*{u}{0}}{\sage{u0}}
    = \myU
  \]

\end{frame}


\section{Complex Eigenvalues and Exponentials}
\subsection{Euler's Formula}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What if $A$ has a complex eigenvalue $\lambda=a+bi$?
  \end{block}

  \pause
  \begin{block}{Problem}
    We need to know how to compute $e^\lambda=e^{a+bi}$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    We may compute $e^{a+bi}$ using the Taylor series representation of $e^t$.
    \begin{align*}
      \onslide<2->{e^{a+bi}
      &=} \onslide<3->{e^a\cdot e^{bi} \\
      &=} \onslide<4->{e^a\cdot\sum_{k=0}^\infty\frac{1}{k!}b^ki^k \\
      &=} \onslide<5->{e^a\cdot\Set*{
        \sum_{k\textnormal{ even}}\frac{1}{k!}b^ki^k
        +
        \sum_{k\textnormal{ odd}}\frac{1}{k!}b^ki^k
        } \\
      &=} \onslide<6->{e^a\cdot\Set*{
        \sum_{k=0}^{\infty}\frac{1}{(2\,k)!}b^{2\,k}i^{2\,k}
        +
        \sum_{k=0}^\infty\frac{1}{(2\,k+1)!}b^{2\,k+1}i^{2\,k+1}
        } \\
      &=} \onslide<7->{e^a\cdot\Set*{
        \sum_{k=0}^{\infty}\frac{(-1)^k}{(2\,k)!}b^{2\,k}
        +
        i\cdot\sum_{k=0}^\infty\frac{(-1)^k}{(2\,k+1)!}b^{2\,k+1}
        } \\
      &=} \onslide<8->{e^a\cdot\Set{\cos(b)+i\sin(b)}}
    \end{align*}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Euler's Formula]
    $e^{a+bi}=e^a\cdot\Set{\cos(b)+i\cdot\sin(b)}$
  \end{theorem}

  \pause
  \begin{corollary}[Parity Formula]
    $e^{a-bi}=e^a\cdot\Set{\cos(-b)+i\cdot\sin(-b)}=e^a\cdot\Set{\cos(b)-i\cdot\sin(b)}$
  \end{corollary}

  \pause
  \begin{corollary}[Euler's Identity]
    $e^{i\pi}=e^0\cdot\Set{\cos(\pi)+i\cdot\sin(\pi)}=-1$
  \end{corollary}

\end{frame}


\subsection{Example}

\begin{sagesilent}
  P = matrix([(I, -I), (1, 1)])
  D = diagonal_matrix([1+I, 1-I])
  A = P*D*P.inverse()
  u0 = matrix.column([0, 2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the initial value problem defined by the data
  \begin{align*}
    A &= \sage{A} & \vv*{u}{0} &= \sage{u0}
  \end{align*}
  \pause Note that $A$ can be diagonalized
  \newcommand{\myPi}{
    \left[
      \begin{array}{rr}
        \frac{-i}{2} & \frac{1}{2} \\
        \frac{i}{2} & \frac{1}{2}
      \end{array}
    \right]
  }
  \[
    \overset{A}{\sage{A}}
    =
    \overset{P}{\sage{P}}
    \overset{D}{\sage{D}}
    \overset{P^{-1}}{\myPi}
  \]
  \pause The solution to our initial value problem is
  \newcommand{\myDt}{
    \left[
      \begin{array}{rr}
        e^t\cdot\Set{\cos(t)+i\sin(t)} & 0 \\
        0                              & e^t\cdot\Set{\cos(t)-i\sin(t)}
      \end{array}
    \right]
  }
  \newcommand{\myU}{
    e^t
    \left[
      \begin{array}{r}
        -2\,\sin(t)\\ 2\,\cos(t)
      \end{array}
    \right]
  }
  \[
    \vv{u}(t)
    = P\exp(Dt)P^{-1}\vv*{u}{0}
    = \myU
  \]

\end{frame}


\end{document}
