\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , shapes
  , shapes.geometric
  , arrows
  , fit
  , calc
  , positioning
  , automata
  , cd
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\title{Matrix Arithmetic}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \tableofcontents[sections={1-4}]
    \column{.5\textwidth}
    \tableofcontents[sections={5-}]
  \end{columns}
\end{frame}


\section{Motivation}
\subsection{What We Know}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{$m\times n$ matrix} is an array of numbers with \mybold{$m$ rows} and
    \myotherbold{$n$ columns}.
  \end{definition}

  \pause

  \begin{block}{Observation}
    We can view any $m\times n$ matrix in two ways
    \gcenter{
      \mybold{a stack of $m$ rows from $\RR^n$} &&
      \myotherbold{a list of $n$ columns from $\RR^m$}
    }
  \end{block}

\end{frame}




\begin{sagesilent}
  set_random_seed(409825)
  A = random_matrix(ZZ, 3, 4)
  r1, r2, r3 = A.rows()
  a1, a2, a3, a4 = A.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    By writing
    \[
      A
      = \sage{A}
      = \begin{bmatrix}\vv*{r}{1}\\ \vv*{r}{2}\\ \vv*{r}{3}\end{bmatrix}
      = \begin{bmatrix}\vv*{a}{1} & \vv*{a}{2} & \vv*{a}{3} & \vv*{a}{4}\end{bmatrix}
    \]
    we define the rows and columns of $A$ as
    \begin{align*}
      \vv*{r}{1} &= \sage{r1} & \vv*{a}{1} &= \sage{a1} \\
      \vv*{r}{2} &= \sage{r2} & \vv*{a}{2} &= \sage{a2} \\
      \vv*{r}{3} &= \sage{r3} & \vv*{a}{3} &= \sage{a3} \\
                 &            & \vv*{a}{4} &= \sage{a4}
    \end{align*}
  \end{example}

\end{frame}





\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Matrices are useful tools for \emph{data storage}.

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , shorten <= 4pt
        , shorten >= 4pt
        , line join=round
        , line cap=round
        , node distance=3cm
        , on grid
        , ultra thick
        , state/.append style={fill=cyan!66}
        , mystyle/.style={->}
        , every node/.style={fill=white}
        , initial/.style={}
        ]

        \node[state] (v1)                {$v_1$};
        \node[state] (v2) [below left =of v1] {$v_2$};
        \node[state] (v3) [below right=of v1] {$v_3$};
        % \node[state] (v5) [below right=of v3] {$v_5$};
        % \node[state] (v6) [below right=of v5] {$v_6$};

        \path
        (v1) edge [mystyle] node {$a_1$} (v2)
        (v1) edge [mystyle] node {$a_2$} (v3)
        (v2) edge [mystyle] node {$a_3$} (v3)
        ;

      \end{tikzpicture}
    \]

    \column{.5\textwidth}
    \[
      A
      =
      \begin{blockarray}{r *{3}{r}}
        \begin{block}{r *{3}{>{$\footnotesize}c<{$}}}
          & $a_1$ & $a_2$ & $a_3$ \\
        \end{block}
        \begin{block}{>{$\footnotesize}l<{$}[*{3}{r}]}
          $v_1$ & -1 & -1 &  0\\
          $v_2$ &  1 &  0 & -1\\
          $v_3$ &  0 &  1 &  1\\
        \end{block}%
      \end{blockarray}%
    \]
  \end{columns}

\end{frame}


\subsection{What We Want}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Can we use matrices for anything other than data storage?
  \end{block}


  \pause
  \begin{block}{Question}
    We can ``do stuff'' with vectors. What about matrices?
  \end{block}

\end{frame}


\section{Basic Matrix Operations}
\subsection{Scalar-Matrix Products}

\begin{sagesilent}
  A = matrix([(2, 0, 10), (-2, 22, -3)])
  c = -6
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    For a scalar $c$ and a $m\times n$ matrix $A$, we define $c\cdot A$ as the
    $m\times n$ matrix whose $(i, j)$th entry is $c\cdot a_{ij}$.
  \end{definition}

  \pause
  \begin{example}
    $(\sage{c})\cdot\sage{A}=\sage{c*A}$
  \end{example}
\end{frame}


\subsection{Matrix Addition}
\begin{sagesilent}
  A = matrix([(-4, -2, -3), (-2, -1, 0)])
  B = matrix([(2, 0, 10), (-2, 22, -3)])
  C = random_matrix(ZZ, 2, 2)
  D = random_matrix(ZZ, 3, 2)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{sum} of two $m\times n$ matrices $A$ and $B$ is the $m\times n$
    matrix $A+B$ whose $(i, j)$th entry is $a_{ij}+b_{ij}$.
  \end{definition}

  \pause

  \begin{example}
    $\sage{A}+\sage{B}=\sage{A+B}$
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Warning}
    The sum $A+B$ is only defined if $A$ and $B$ have the same size.
  \end{block}

  \pause

  \begin{example}
    $\sage{C}+\sage{D}$ is not defined!
  \end{example}
\end{frame}



\section{Matrix-Vector Products}
\subsection{The ``Column Picture''}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    A \emph{linear combination} of $\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}$
    is a sum of the form
    \[
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_n\cdot\vv*{v}{n}
    \]
    where $c_1, c_2, \dotsc, c_n$ are scalars.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      Can we set weights $x_1$, $x_2$, and $x_3$ so that the weighted net flow
      through the nodes is given by $\vv{b}$?
    \end{block}
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , shorten <= 4pt
        , shorten >= 4pt
        , line join=round
        , line cap=round
        , node distance=2cm
        , on grid
        , ultra thick
        , state/.append style={fill=cyan!66}
        , mystyle/.style={->}
        , every node/.style={fill=white}
        , initial/.style={}
        ]

        \node[state] (v1)                {$v_1$};
        \node[state] (v2) [right =3cm of v1] {$v_2$};
        \node[state] (v3) [above right=2cm and 1.5cm of v1] {$v_3$};

        \path
        (v1) edge [mystyle] node {$a_1$} (v2)
        (v2) edge [mystyle] node {$a_2$} (v3)
        (v1) edge [mystyle] node {$a_3$} (v3)
        ;
      \end{tikzpicture}
    \]
  \end{columns}
  \pause
  \begin{block}{Answer}
    To answer this question, we must solve
    \[
      x_1\cdot\vv*{a}{1}+x_2\cdot\vv*{a}{2}+x_3\cdot\vv*{a}{3}=\vv{b}
    \]
    for the ``unknown'' scalars $x_1$, $x_2$, and $x_3$. \pause The answer to
    the question is ``yes'' if $\vv{b}$ is a \emph{linear combination} of the
    incidence vectors.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Matrix-Vector Multiplication, ``Column Picture'']
    Consider a $m\times n$ matrix $A$ and a vector $\vv{v}\in\RR^n$ of the form
    \begin{align*}
      A &= \begin{bmatrix}\vv*{a}{1} & \vv*{a}{2} & \dotsb & \vv*{a}{n}\end{bmatrix} & \vv{v} &= \begin{bmatrix}v_1 \\ v_2 \\ \vdots \\ v_n\end{bmatrix}
    \end{align*}\pause
    The \emph{matrix-vector product} $A\vv{v}$ is the linear combination
    \[
      A\vv{v}=v_1\cdot\vv*{a}{1}+v_2\cdot\vv*{a}{2}+\dotsb+v_n\cdot\vv*{a}{n}
    \]\pause
    Note that $A\vv{v}$ is a \emph{vector} in $\RR^m$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(84095844279)
  A = random_matrix(ZZ, 2, 3)
  a1, a2, a3 = map(matrix.column, A.columns())
  v = vector([3, -2, 7])
  v1, v2, v3 = v
  vc = matrix.column(v)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vector $\vv{v}$ given by
    \begin{align*}
      A &= \sage{A} & \vv{v} &= \sage{vc}
    \end{align*}
    \onslide<2->{The ``column picture'' of matrix-vector products gives}
    \begin{align*}
      \onslide<3->{A\vv{v}
      &=} \onslide<4->{\sage{v1}\sage{a1}+(\sage{v2})\sage{a2}+\sage{v3}\sage{a3} \\
      &=} \onslide<5->{\sage{v1*a1}+\sage{v2*a2}+\sage{v3*a3} \\
      &=} \onslide<6->{\sage{A*vc}}
    \end{align*}

  \end{example}

\end{frame}





\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      Can we set weights $x_1$, $x_2$, and $x_3$ so that the weighted net flow
      through the nodes is given by $\vv{b}$?
    \end{block}
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , shorten <= 4pt
        , shorten >= 4pt
        , line join=round
        , line cap=round
        , node distance=2cm
        , on grid
        , ultra thick
        , state/.append style={fill=cyan!66}
        , mystyle/.style={->}
        , every node/.style={fill=white}
        , initial/.style={}
        ]

        \node[state] (v1)                {$v_1$};
        \node[state] (v2) [right =3cm of v1] {$v_2$};
        \node[state] (v3) [above right=2cm and 1.5cm of v1] {$v_3$};

        \path
        (v1) edge [mystyle] node {$a_1$} (v2)
        (v2) edge [mystyle] node {$a_2$} (v3)
        (v1) edge [mystyle] node {$a_3$} (v3)
        ;
      \end{tikzpicture}
    \]
  \end{columns}
  \pause
  \begin{block}{Answer}
    To answer this question, we must solve
    \[
      x_1\cdot\vv*{a}{1}+x_2\cdot\vv*{a}{2}+x_3\cdot\vv*{a}{3}=\vv{b}
    \]
    for the ``unknown'' scalars $x_1$, $x_2$, and $x_3$. \pause This can be
    written as one ``matrix equation'' $A\vv{x}=\vv{b}$ where $A$ is the
    incidence matrix and $\vv{x}=\langle x_1, x_2, x_3\rangle$.
  \end{block}

\end{frame}


\subsection{The ``Row Picture''}

\begin{sagesilent}
  set_random_seed(84095844279)
  A = random_matrix(ZZ, 2, 3)
  a1, a2, a3 = map(matrix.column, A.columns())
  v = vector([3, -2, 7])
  v1, v2, v3 = v
  vc = matrix.column(v)
  r1, r2 = A.rows()
  a11, a12, a13 = r1
  a21, a22, a23 = r2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
  Consider $A\vv{v}$ where $A=\sage{A}$ and $\vv{v}=\sage{v}$.
  \begin{align*}
    \onslide<2->{A\vv{v}
    &=} \onslide<3->{\sage{v1}\sage{a1}+(\sage{v2})\sage{a2}+\sage{v3}\sage{a3} \\
    &=} \onslide<4->{\begin{bmatrix}(\sage{v1})(\sage{a11})+(\sage{v2})(\sage{a12})+(\sage{v3})(\sage{a13}) \\ (\sage{v1})(\sage{a21})+(\sage{v2})(\sage{a22})+(\sage{v3})(\sage{a23})\end{bmatrix} \\
    &=} \onslide<5->{\begin{bmatrix}\sage{r1}\cdot\sage{v}\\ \sage{r2}\cdot\sage{v}\end{bmatrix} \\
    &=} \onslide<6->{\begin{bmatrix}\vv*{r}{1}\cdot\vv{v}\\ \vv*{r}{2}\cdot\vv{v}\end{bmatrix}}
  \end{align*}
  \onslide<6->{where $\vv*{r}{1}$ and $\vv*{r}{2}$ are the rows of $A$. }%
  \onslide<7->{Matrix-vector products can be computed with dot products!}
\end{block}


\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Matrix-Vector Multiplication, ``Row Picture'']

    The \emph{matrix-vector product} of $A\in\RR^{m\times n}$ and
    $\vv{v}\in\RR^n$ is
    \[
      A\vv{v}
      =
      \begin{bmatrix}
        \vv*{r}{1}\cdot\vv{v} \\
        \vv*{r}{2}\cdot\vv{v} \\
        \vdots \\
        \vv*{r}{m}\cdot\vv{v}
      \end{bmatrix}
    \]
    where $\vv*{r}{1}, \vv*{r}{2}, \dotsc, \vv*{r}{m}$ are the rows of $A$.
  \end{definition}

  \pause This is equivalent to the ``column picture.'' \pause In particular,
  $A\vv{v}$ is a \emph{vector} in $\RR^m$.

\end{frame}



\begin{sagesilent}
  set_random_seed(388)
  A = random_matrix(ZZ, 3, 2)
  v = vector([7, -2])
  vc = matrix.column(v)
  r1, r2, r3 = A.rows()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vector $\vv{v}$ given by
    \begin{align*}
      A &= \sage{A} & \vv{v} &= \sage{vc}
    \end{align*}\pause
    The ``row picture'' of matrix-vector products gives
    \[
      A\vv{v}
      =\pause
      \begin{bmatrix}
        \sage{r1}\cdot\sage{v} \\
        \sage{r2}\cdot\sage{v} \\
        \sage{r3}\cdot\sage{v}
      \end{bmatrix}
      = \pause\sage{A*vc}
    \]
  \end{example}

\end{frame}



\subsection{Matrices are Machines}

\begin{sagesilent}
  set_random_seed(482095)
  A = random_matrix(ZZ, 2, 3)
  v = random_matrix(ZZ, 2, 1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{More than Data Storage}
    A $m\times n$ matrix $A$ is a machine
    \[
      \begin{tikzcd}[row sep=tiny]
        \RR^n\arrow{r}{A} \pgfmatrixnextcell \RR^m %\\        \vv{v}\in\RR^n\arrow[mapsto]{r}\pgfmatrixnextcell A\vv{v}\in\RR^m
      \end{tikzcd}
    \]\pause
    that transforms vectors $\vv{v}\in\pause\RR^n$ \pause into vectors
    $A\vv{v}\in\pause\RR^m$.
  \end{block}

  \pause
  \begin{block}{Warning}
    $A\vv{v}$ only makes sense when
    $\vv{v}\in\pause\RR^{\#\textnormal{columns}(A)}$
  \end{block}

  \pause
  \begin{example}
    $\sage{A}\sage{v}$ does not make sense
  \end{example}


\end{frame}



\begin{sagesilent}
  v = vector([1, 2, -1])
  vc = matrix.column(v)
  A = matrix([[6, -7, 5],[2, -3, 4]])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The matrix-vector product
  \[
    \underset{A}{\sage{A}}\underset{\vv{v}}{\sage{vc}}=\underset{A\vv{v}}{\sage{A*vc}}
  \]
  can be interpreted geometrically.
  \newcommand{\myInputVector}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, scale=1]

      \draw[very thick, ->, blue] (0, 0, 0) -- (1/2, 1, -1) node [below] {$\vv{v}$};

      \draw[ultra thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
      \draw[ultra thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
      \draw[ultra thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};
    \end{tikzpicture}
  }
  \newcommand{\myOutputVectors}{
    \begin{tikzpicture}[line cap=round, line join=round, scale=1/2]

      % \draw[very thick, purple, ->] (0, 0) -- (1, 2) node [right] {$\vv*{w}{1}$};
      \draw[very thick, violet, ->] (0, 0) -- (-3, -2) node [below] {$A\vv{v}$};

      \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
      \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};
    \end{tikzpicture}
  }
  \[
    \begin{tikzcd}
      \myInputVector\arrow{r}{A} \pgfmatrixnextcell\myOutputVectors
    \end{tikzcd}
  \]

\end{frame}



\section{Matrix Products}
\subsection{Idea}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What if we want to ``transform'' a vector twice?
    \[
      \begin{tikzcd}[row sep=tiny]
        \RR^n\arrow{r}{B}\pgfmatrixnextcell\RR^m\arrow{r}{A}\pgfmatrixnextcell \RR^\ell \\
        \vv{v}\in\RR^n\arrow[mapsto]{r}\pgfmatrixnextcell B\vv{v}\in\RR^m\arrow[mapsto]{r}\pgfmatrixnextcell (AB)\vv{v}\in\RR^\ell
      \end{tikzcd}
    \]
  \end{block}

  \pause
  \begin{block}{Answer}
    The ``matrix product'' $AB$ is a machine that transforms $\vv{v}\in\RR^n$
    into $B\vv{v}\in\pause\RR^m$ and then into $(AB)\vv{v}\in\pause\RR^\ell$.
  \end{block}


\end{frame}


\subsection{Criterion}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Test to Determine if $AB$ is Defined}
    The \emph{matrix-matrix product} $AB$ is only defined when
    \[
      \textnormal{number of columns of }A=\textnormal{number of rows of }B
    \]\pause
    The product $AB$ is a new matrix whose size is
    \[
      (\textnormal{number of rows of }A)\times(\textnormal{number of columns of }B)
    \]
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(49151)
  A = random_matrix(ZZ, 2, 3)
  mA, nA = A.dimensions()
  B = random_matrix(ZZ, 3, 4)
  mB, nB = B.dimensions()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{example}
    Consider the matrices $A$ and $B$ given by
    \begin{align*}
      A &= \sage{A} & B &= \sage{B}
    \end{align*}\pause
    These matrices can be used to form matrix products.\pause
    \begin{center}
      \begin{tabular}{cccc}
        \multicolumn{1}{l}{Product} & \multicolumn{1}{l}{Dimensions} & \multicolumn{1}{l}{Defined?} & \multicolumn{1}{l}{Result}\\
        \hline\pause
        $AB$ &\pause $(\sage{mA}\times\sage{nA})(\sage{mB}\times\sage{nB})$ &\pause \cmark &\pause $\sage{mA}\times\sage{nB}$\\ \pause
        $BA$ &\pause $(\sage{mB}\times\sage{nB})(\sage{mA}\times\sage{nA})$ &\pause \xmark &\pause N/A\\ \pause
        $B^\intercal A$&\pause $(\sage{nB}\times\sage{mB})(\sage{mA}\times\sage{nA})$ &\pause \xmark &\pause N/A\\ \pause
        $B^\intercal A^\intercal$&\pause $(\sage{nB}\times\sage{mB})(\sage{nA}\times\sage{mA})$ &\pause \cmark &\pause $\sage{nB}\times\sage{mA}$
      \end{tabular}
    \end{center}
  \end{example}


\end{frame}



\subsection{The ``Column Picture''}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Matrix-Matrix Multiplication, ``Column Picture'']
    Consider a $m\times n$ matrix $A$ and a $n\times \ell$ matrix $B$.  The
    \emph{matrix product} $AB$ is the $m\times \ell$ matrix given by
    \[
      AB
      = \begin{bmatrix}A\vv*{b}{1} &A\vv*{b}{2} &
        \dotsb&A\vv*{b}{\ell}\end{bmatrix}
    \]
    where $\vv*{b}{1}, \vv*{b}{2}, \dotsc,\vv*{b}{\ell}$ are the columns of $B$.
  \end{definition}

\end{frame}



\begin{sagesilent}
  set_random_seed(4915879427)
  A = random_matrix(ZZ, 4, 3)
  B = random_matrix(ZZ, 3, 2)
  b1, b2 = map(matrix.column, B.columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrices $A$ and $B$ given by
    \begin{align*}
      A &= \sage{A} & B &= \sage{B}
    \end{align*}
    \onslide<2->{Multiplying $A$ by each column of $B$ gives the columns of $AB$.}
    \begin{align*}
      \onslide<3->{A\vv*{b}{1} &=} \onslide<4->{\sage{A*b1}} & \onslide<5->{A\vv*{b}{2} &=} \onslide<6->{\sage{A*b2}} & \onslide<7->{AB &=} \onslide<8->{\sage{A*B}}
    \end{align*}
  \end{example}

\end{frame}


\subsection{The ``Row Picture''}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Matrix-Matrix Multiplication, ``Row Picture'']
    The \emph{product} of a $m\times n$ matrix $A$ and a $n\times \ell$ matrix
    $B$ is the $m\times \ell$ matrix $AB$ whose $(i, j)$th entry is the dot
    product
    \[
      (\textnormal{$i$th row of $A$})\cdot(\textnormal{$j$th column of $B$})
    \]\pause
    This definition makes sense because the rows of $A$ and the columns of $B$
    are vectors in \pause$\RR^n$.
  \end{definition}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \[
    \input{./matmult.tex}
    \]
  \end{example}
\end{frame}


\section{Properties}
\subsection{General Properties}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties}
    Matrix arithmetic obeys the following laws.
    \begin{description}[multiplicative associativity]
    \item<2->[additive associativity] $A+(B+C)=(A+B)+C$
    \item<3->[multiplicative associativity] $(AB)C=A(BC)$
    \item<4->[additive commutativity] $A+B=B+A$
    \item<5->[left distributive] $A(B+C)=AB+AC$
    \item<6->[right distributive] $(A+B)C=AC+BC$
    \end{description}
  \end{block}
\end{frame}


\subsection{Noncommutativity}
\begin{sagesilent}
  A = matrix(ZZ, 2, [1, 0, 0, 0])
  B = matrix(ZZ, 2, [0, 1, 0, 0])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \begin{align*}
      \onslide<2->{\sage{A}\sage{B} &=} \onslide<3->{\sage{A*B}} &
      \onslide<4->{\sage{B}\sage{A} &=} \onslide<5->{\sage{B*A}}
    \end{align*}
  \end{example}

  \begin{block}{\onslide<6->{Important Observation}}
    \onslide<6->{In general, $AB\neq BA$. Matrix multiplication is not
      commutative!}
  \end{block}
\end{frame}


\subsection{Transposition}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties}
    Matrix transposition satisfies the following properties.
    \begin{description}[reverse multiplicative]
    \item<+->[additivity] $(A+B)^\intercal=A^\intercal+B^\intercal$
    \item<+->[involution] $(A^\intercal)^\intercal=A$
    \item<+->[scalar linearity] $(c\cdot A)^\intercal=c\cdot A^\intercal$
    \item<+->[reverse multiplicative] $(AB)^\intercal=B^\intercal A^\intercal$
    \end{description}
    \onslide<+->{``Transposition is an \emph{order-reversing involution}.''}
  \end{block}
\end{frame}






\section{Vocabulary}
\subsection{Square Matrices}


\begin{sagesilent}
  A = random_matrix(ZZ, 2)
  B = random_matrix(ZZ, 3)
  C = random_matrix(ZZ, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix is \emph{square} if it is of size $n\times n$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{A} && \sage{B} && \sage{C}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Column and Row Matrices}


\begin{sagesilent}
  c1 = random_matrix(ZZ, 2, 1)
  c2 = random_matrix(ZZ, 3, 1)
  c3 = random_matrix(ZZ, 4, 1)
  c4 = random_matrix(ZZ, 5, 1)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{column matrix} is a $n\times 1$ matrix.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{c1} && \sage{c2} && \sage{c3} && \sage{c4}
    \end{align*}
  \end{example}
\end{frame}


\begin{sagesilent}
  r1 = random_matrix(ZZ, 1, 2)
  r2 = random_matrix(ZZ, 1, 3)
  r3 = random_matrix(ZZ, 1, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{row matrix} is a $1\times n$ matrix.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{r1} && \sage{r2} && \sage{r3}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Diagonal Matrices}


\begin{sagesilent}
  D1 = matrix.diagonal([ZZ.random_element() for x in range(2)])
  D2 = matrix.diagonal([ZZ.random_element() for x in range(3)])
  D3 = matrix.diagonal([ZZ.random_element() for x in range(4)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{diagonal matrix} is a square matrix $A$ whose entries satisfy
    $a_{ij}=0$ if $i\neq j$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{D1} && \sage{D2} && \sage{D3}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Upper and Lower Triangular}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An \emph{upper triangular matrix} is a matrix $A$ whose entries satisfy
    $a_{ij}=0$ if $i>j$.
  \end{definition}

  \onslide<2->{
    \begin{example}}
    \begin{align*}
      \input{./upper1.tex} && \input{./upper2.tex}
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{lower triangular matrix} is a matrix $A$ whose entries satisfy
    $a_{ij}=0$ if $i<j$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \input{./lower1.tex} && \input{./lower2.tex}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Symmetric Matrices}

\begin{sagesilent}
  Q3 = random_quadraticform(ZZ, 3, [-10,10]).matrix()
  Q4 = random_quadraticform(ZZ, 4, [-10,10]).matrix()
  Q5 = random_quadraticform(ZZ, 5, [-10,10]).matrix()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix $A$ is \emph{symmetric} if $A^\intercal=A$.
  \end{definition}

  \pause

  \begin{example}
    {\scriptsize
      \begin{align*}
        \sage{Q3} && \sage{Q4} && \sage{Q5}
      \end{align*}}
  \end{example}
\end{frame}


\subsection{Zero Matrices}

\begin{sagesilent}
  A = zero_matrix(3)
  B = zero_matrix(3, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{$m\times n$ zero matrix} is the matrix $O_{m\times n}$ whose
    entries are all zeros.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      O_{3\times 3} &= \sage{A} & O_{3\times 4} &= \sage{B}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Identity Matrices}

\begin{sagesilent}
  I2 = identity_matrix(2)
  I3 = identity_matrix(3)
  I4 = identity_matrix(4)
  A = random_matrix(ZZ, 2, 3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{$n\times n$ identity matrix} is the matrix $I_n$ whose $(i, j)$th entry is given by
    \[
    \begin{cases}
      1 & i=j \\
      0 & i\neq j
    \end{cases}
    \]
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      I_2 &= \sage{I2} &
      I_3 &= \sage{I3} &
      I_4 &= \sage{I4}
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every $m\times n$ matrix $A$ satisfies $A=AI_n=I_mA$.
  \end{theorem}

  \pause

  \begin{example}
    \begin{align*}
      \sage{A}
      &= \sage{A}\sage{I3} \\
      &= \sage{I2}\sage{A}
    \end{align*}
  \end{example}
\end{frame}



\end{document}
