\documentclass[usenames, dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , angles
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , patterns
  , positioning
  , quotes
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%


\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%
\DeclarePairedDelimiter\inner{\langle}{\rangle}%

% \newcommand{\semitransp}[2][35]{\color{fg!#1}#2}

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\gm}{gm}
\DeclareMathOperator{\am}{am}
\DeclareMathOperator{\trace}{trace}
\DeclareMathOperator{\proj}{proj}
\DeclareMathOperator{\diag}{diag}


\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}


\newcommand{\smallsage}[1]{{\small\sage{#1}}}
\newcommand{\footsage}[1]{{\footnotesize\sage{#1}}}
\newcommand{\scriptsage}[1]{{\scriptstyle\sage{#1}}}
\newcommand{\tinysage}[1]{{\tiny\sage{#1}}}


\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Definiteness of Quadratic Forms}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={1-}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}


\section{Quadratic Forms}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{quadratic form} on $\mathbb{R}^n$ is a function
    $Q:\mathbb{R}^n\to\mathbb{R}$ given by
    \[
      Q(x_1, x_2, \dotsc, x_n) = \sum_{i\geq j\geq 0} c_{ij}\cdot x_ix_j
    \]
  \end{definition}

\end{frame}




\begin{frame}[fragile]

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{sagesilent}
    set_random_seed(98720)
    var('x y')
    X = random_matrix(ZZ, 3, 2)
    Y = random_matrix(ZZ, 3, 2)
    A = X.T*X - Y.T*Y
    x = vector([x, y])
    Q = expand(x*A*x)\end{sagesilent}
  \begin{example}[Quadratic Form on $\mathbb{R}^{\sage{A.nrows()}}$]
    $Q\sage{tuple(x)} = \sage{Q}$
  \end{example}

  \pause
  \begin{sagesilent}
    set_random_seed(748)
    var('x1 x2 x3')
    X = random_matrix(ZZ, 1, 3)
    Y = random_matrix(ZZ, 1, 3)
    A = X.T*X-Y.T*Y
    x = vector([x1, x2, x3])
    Q = expand(x*A*x)\end{sagesilent}
  \begin{example}[Quadratic Form on $\mathbb{R}^{\sage{A.nrows()}}$]
    $Q{\sage{tuple(x)}} = \sage{Q}$
  \end{example}

  \pause
  \begin{example}
    $Q(x, y)=x^2-y^2+1$ is \emph{not} a quadratic form
  \end{example}

\end{frame}


\subsection{Definiteness}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Every quadratic form satisfies $Q(\vv{O})=0$.
  \end{block}

  \pause
  \begin{example}
    For $Q(x, y)=x^2+y^2-xy$ we have $Q(\vv{O})=0^2+0^2-(0)(0)=0$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A quadratic form $Q$ on $\mathbb{R}^n$ is
    \begin{description}[negative semidefinite]
    \item<2->[positive definite] if $Q(\vv{x})>0$ for $\vv{x}\neq\vv{O}$
    \item<3->[negative definite] if $Q(\vv{x})<0$ for $\vv{x}\neq\vv{O}$
    \item<4->[positive semidefinite] if $Q(\vv{x})\geq0$ for $\vv{x}\neq\vv{O}$
    \item<5->[negative semidefinite] if $Q(\vv{x})\leq0$ for $\vv{x}\neq\vv{O}$
    \end{description}
    \onslide<6->{Otherwise $Q$ is \emph{indefinite}.}
  \end{definition}

\end{frame}



\begin{sagesilent}
  var('x y')
  x = vector([x, y])
  A = identity_matrix(2)
  Q = expand(x*A*x)
\end{sagesilent}
\begin{frame}[fragile]

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{example}
    Consider the quadratic form on $\mathbb{R}^{\sage{A.nrows()}}$ given by
    \[
      Q\sage{tuple(x)}=\sage{Q}
    \]\pause
    For $\vv{x}\neq\sage{zero_vector(2)}$, this quadratic form satisfies
    \[
      Q\sage{tuple(x)}=\sage{Q}>0
    \]\pause
    This quadratic form is \pause \emph{positive definite}.
  \end{example}

\end{frame}


\begin{sagesilent}
  var('x y')
  x = vector([x, y])
  A = ones_matrix(2)
  Q = expand(x*A*x)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the quadratic form on $\mathbb{R}^{\sage{A.nrows()}}$ given by
    \[
      Q\sage{tuple(x)}=\sage{Q}
    \]\pause
    For $\vv{x}\neq\sage{zero_vector(2)}$, this quadratic form satisfies
    \[
      Q\sage{tuple(x)}
      = \pause\sage{Q}
      = \pause\sage{factor(Q)}
      \geq 0
    \]\pause
    This quadratic form is \pause \emph{positive semidefinite}.
  \end{example}

\end{frame}




\begin{sagesilent}
  set_random_seed(47920)
  var('x1 x2 x3')
  x = vector([x1, x2, x3])
  X = random_matrix(ZZ, 2, 3)
  Y = random_matrix(ZZ, 1, 3)
  A = X.T*X-Y.T*Y
  Q = expand(x*A*x)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the quadratic form on $\mathbb{R}^{\sage{A.nrows()}}$ given by
    \[
      Q\sage{tuple(x)}=\sage{Q}
    \]\pause
    Finding definiteness is hard!
  \end{example}

\end{frame}



\subsection{Symmetric Matrices}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every quadratc form $Q$ on $\mathbb{R}^n$ may be uniquely written as
    \[
      Q(\vv{x})
      = \vv{x}\cdot S\vv{x}
      = \vv{x}^\intercal S\vv{x}
    \]
    where $S$ is $n\times n$ symmetric and
    $\vv{x}=\langle x_1, x_2, \dotsc, x_n\rangle$.
  \end{theorem}

  \pause
  \begin{block}{Formula}
    For $Q(x_1, \dotsc, x_n)=\sum c_{ij}\cdot x_ix_j$ the entries of $S$ are
    \[
      s_{ij}
      =
      \begin{cases}
        c_{ij} & i=j \\
        \frac{c_{ij}}{2} & i\neq j
      \end{cases}
    \]
    This gives $Q(\vv{x})=\vv{x}\cdot S\vv{x}$.
  \end{block}

\end{frame}


\begin{sagesilent}
  var('x1 x2 x3')
  S = matrix([(-3, -1, 7), (-1, -4, -5), (7, -5, 1)])
  x = vector([x1, x2, x3])
  Q = expand(x*S*x)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the quadratic form on $\mathbb{R}^{\sage{S.nrows()}}$ given by
    \[
      Q\sage{tuple(x)} = \sage{Q}
    \]\pause
    Then $Q(\vv{x})=\vv{x}\cdot S\vv{x}$ where
    \[
      S = \pause\sage{S}
    \]
  \end{example}

\end{frame}



\newcommand{\myQuadFormsText}{
  \begin{array}{c}
    \textnormal{Quadratic Forms} \\
    \textnormal{on }\mathbb{R}^{n}
  \end{array}}
\newcommand{\mySymmText}{
  \begin{array}{c}
    n\times n\textnormal{ Symmetric} \\
    \textnormal{Matrices}
  \end{array}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    There is a correspondance
    \[
      \Set*{\myQuadFormsText}\leftrightarrow
      \Set*{\mySymmText}
    \]
    We can translate interesting statements about quadratic forms into
    statements about symmetric matrices (and vice-versa).
  \end{block}

\end{frame}


\subsection{Completing the Square}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The Spectral Theorem says that every Hermitian matrix factors as
    \[
      S=PDP^\ast
    \]
    where $P$ is \emph{unitary} ($P^\ast=P^{-1}$).
  \end{block}

  \pause
  \begin{block}{Note}
    If $S$ is real-symmetric, then this factorization is
    \[
      S=PDP^\intercal
    \]
    where $P$ is \emph{orthogonal} ($P^\intercal=P^{-1}$).
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{How to "Complete the Square"}
    Given a spectral decomposition $S=PDP^\intercal$, let
    $\vv{y}=P^\intercal \vv{x}$. \onslide<2->{Our quadratic form then becomes}
    \begin{align*}
      \onslide<3->{Q(\vv{x})
      &=} \onslide<4->{\vv{x}\cdot S\vv{x} \\
      &=} \onslide<5->{\vv{x}^\intercal PDP^\intercal\vv{x} \\
      &=} \onslide<6->{(P^\intercal\vv{x})^\intercal D(P^\intercal\vv{x}) \\
      &=} \onslide<7->{\vv{y}^\intercal D\vv{y} \\
      &=} \onslide<8->{\lambda_1\cdot y_1^2+\lambda_2\cdot y_2^2+\dotsb+\lambda_n\cdot y_n^2}
    \end{align*}
    \onslide<9->{The eigenvalues of $S$ determine the definiteness of $Q$!}
  \end{block}

\end{frame}



\subsection{Classifying Quadratic Forms}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $Q$ be a quadratic form given by $Q(\vv{x})=\vv{x}\cdot S\vv{x}$. Then
    $Q$ is
    \begin{description}[negative semidefinite]
    \item<1->[positive definite] $\iff$ the e-vals of $S$ are $>0$
    \item<2->[negative definite] $\iff$ the e-vals of $S$ are $<0$
    \item<3->[positive semidefinite] $\iff$ the e-vals of $S$ are $\geq0$
    \item<4->[negative semidefinite] $\iff$ the e-vals of $S$ are $\leq0$
    \item<5->[indefinite] $\iff$ $S$ has positive and negative e-values
    \end{description}
  \end{theorem}

  \onslide<6->
  \begin{block}{Convention}
    The \emph{definiteness} of $S$ is the definiteness of
    $Q(\vv{x})=\vv{x}\cdot S\vv{x}$.
  \end{block}

\end{frame}


\begin{sagesilent}
  S = matrix([(-1, 1, 0), (1, -1, 0), (0, 0, -2)])
  var('x y z t')
  x = vector([x, y, z])
  Q = expand(x*S*x)
  chi = t*identity_matrix(S.nrows())-S
  latex.matrix_delimiters(left='|', right='|')
  from functools import partial
  elem = partial(elementary_matrix, S.nrows())
  E1 = elem(row1=0, row2=1, scale=1)
  E2 = elem(row1=0, scale=1/t)
  E3 = elem(row1=1, row2=0, scale=1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the quadratic form given by
    \[
      Q\sage{tuple(x)}=\sage{Q}
    \]
    \onslide<2->{The characteristic polynomial of the matrix $S$ representing $Q$ is}
    \begin{align*}
      \onslide<3->{\chi_S(t)
      &=} \onslide<4->{\footsage{chi}
        =} \onslide<5->{\footsage{E1*chi} \\
      &=} \onslide<6->{t\cdot\footsage{E2*(E1*chi)}
        =} \onslide<7->{t\cdot\footsage{E3*(E2*(E1*chi))} \\
      &=} \onslide<8->{\sage{factor(S.characteristic_polynomial(t))}}
    \end{align*}
    \onslide<9->{This means that
      $\operatorname{E-Vals}(S)=\sage{Set(S.eigenvalues())}$. }\onslide<10->{The
      quadratic form is \emph{negative semidefinite}.}
  \end{example}

\end{frame}


\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  S = matrix([(4, -2, 1), (-2, 4, 1), (1, 1, 1)])
  var('x1 x2 x3')
  x = vector([x1, x2, x3])
  Q = expand(x*S*x)
  J, P = S.jordan_form(transformation=True, subdivide=False)
  var('y1 y2 y3')
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the quadratic form given by
  \[
    Q\sage{tuple(x)}=\sage{Q}
  \]
  \onslide<2->{A spectral decomposition of the matrix $S$ representing $Q$ is}
  \newcommand{\myP}{\tiny
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{6}} \\
        -\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{6}} \\
        0 & \frac{1}{\sqrt{3}} & -\frac{2}{\sqrt{6}}
      \end{array}
    \right]
  }
  \newcommand{\myPT}{\tiny
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}} & 0 \\
        \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{3}}  & \frac{1}{\sqrt{3}} \\
        \frac{1}{\sqrt{6}} & \frac{1}{\sqrt{6}}  & -\frac{2}{\sqrt{6}}
      \end{array}
    \right]
  }
  \onslide<3->
  \[
    \overset{S}{\tinysage{S}}
    =
    \overset{P}{\myP}
    \overset{D}{\tinysage{J}}
    \overset{P^\intercal}{\myPT}
  \]
  \onslide<4->{Put $\vv{y}=P^\intercal\vv{x}$. } \onslide<5->{Then}
  \begin{align*}
    \onslide<5->{Q(x_1, x_2, x_3)
    &=} \onslide<6->{6\,y_1^2+3\,y_2^2+0\,y_3^2 \\
    &=} \onslide<7->{6\cdot\Set*{\frac{1}{\sqrt{2}}\cdot(x_1-x_2)}^2
      +3\cdot\Set*{\frac{1}{\sqrt{3}}\cdot(x_1+x_2+x_3)}^2 \\
    &=} \onslide<8->{3\cdot(x_1-x_2)^2+(x_1+x_2+x_3)^2}
  \end{align*}
  \onslide<9->{By ``completing the square'' we find that $Q$ is \emph{positive
      semidefinite}.}

\end{frame}


\section{Positive Semidefinite Matrices and Gramians}
\subsection{Gramians are Positve Semidefinite}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The \emph{Gramian} of $A$ is $A^\intercal A$.
  \end{block}

  \pause
  \begin{block}{Note}
    The Gramian of an $m\times n$ matrix is an $n\times n$ symmetric matrix.
  \end{block}

  \pause
  \begin{block}{Question}
    What is the definiteness of the Gramian?
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The Gramian of an $m\times n$ matrix $A$ is positive semidefinite.
  \end{theorem}\pause
  \begin{proof}
    $Q(\vv{x})
    = \vv{x}^\intercal (A^\intercal A)\vv{x}
    = (A\vv{x})^\intercal(A\vv{x})
    = \norm{A\vv{x}}^2\geq0$
  \end{proof}

\end{frame}


\subsection{Positive Semidefinite Matrices are Gramians}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Suppose $S$ is positive semidefinite. Is it possible to represent $S$ as a
    Gramian $S=A^\intercal A$?
  \end{block}

  \pause
  \begin{block}{Answer}
    Consider a spectral decomposition $S=PDP^\intercal$. This gives
    \begin{align*}
      S
      = PDP^\intercal
      = P\sqrt{D}\sqrt{D}P^\intercal
      = (\sqrt{D}P^\intercal)^\intercal(\sqrt{D}P^\intercal)
      = A^\intercal A
    \end{align*}\pause
    Here, $\sqrt{D}$ is defined as
    \[
      \sqrt{D}
      =
      \left[
        \begin{array}{cccc}
          \sqrt{\lambda_1} &&& \\
                           & \sqrt{\lambda_2} \\
                           &                 & \ddots \\
                           &                 &        & \sqrt{\lambda_n}
        \end{array}
      \right]
    \]
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every positive semidefinite matrix $S$ is a Gramian $S=A^\intercal A$.
  \end{theorem}

  \pause
  \begin{block}{Note}
    Given a spectral decomposition $S=PDP^\intercal$, we may take
    $A=\sqrt{D}P^\intercal$ to obtain $S=A^\intercal A$.
  \end{block}

\end{frame}




\begin{sagesilent}
  S = matrix([(6, 2), (2, 9)])
  D, P = S.jordan_form(subdivide=False, transformation=True)
  P = matrix.column(map(lambda v: v.normalized(), P.columns()))
  sqrtD = diagonal_matrix([sqrt(d) for d in D.diagonal()])
  A = (sqrtD*P.T).canonicalize_radical()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the spectral decomposition
    \newcommand{\myP}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{5}} & \frac{2}{\sqrt{5}} \\
          \frac{2}{\sqrt{5}} & -\frac{1}{\sqrt{5}}
        \end{array}
      \right]
    }
    \newcommand{\myPT}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{5}} & \frac{2}{\sqrt{5}} \\
          \frac{2}{\sqrt{5}} & -\frac{1}{\sqrt{5}}
        \end{array}
      \right]
    }
    \[
      \overset{S}{\sage{S}}
      =
      \overset{P}{\myP}
      \overset{D}{\sage{D}}
      \overset{P^\intercal}{\myPT}
    \]\pause
    Defining $A=\sqrt{D}P^\intercal$ gives
    \[
      A
      = \overset{\sqrt{D}}{\sage{sqrtD}}\overset{P^\intercal}{\myPT}
      = \sage{A}
    \]\pause
    We can check our work by computing
    \[
      A^\intercal A
      = \sage{A.T}\sage{A}
      = \sage{A.T*A}
    \]
  \end{example}

\end{frame}



\section{Positive Definiteness and the Cholesky Decomposition}
\subsection{Definiteness from $LU$-Pivots}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    The eigenvalues determine the definiteness of $S$. Finding eigenvalues is
    \emph{hard}.
  \end{block}

  \pause
  \begin{block}{Question}
    How can we efficiently determine the definiteness of a matrix?
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $S$ be a symmetric matrix. Consider an $S=LU$ factorization. Then $S$ is
    \begin{description}[negative semidefinite]
    \item<2->[positive definite] $\iff$ the pivots in $U$ are $>0$
    \item<3->[negative definite] $\iff$ the pivots in $U$ are $<0$
    \item<4->[positive semidefinite] $\iff$ pivots in $U$ are $\geq0$
    \item<5->[negative semidefinite] $\iff$ pivots in $U$ are $\leq0$
    \item<6->[indefinite] $\iff$ $U$ has positive and negative pivots
    \end{description}
  \end{theorem}

  \onslide<7->
  \begin{block}{Warning}
    This requires that we have an $S=LU$ decomposition, not a $PS=LU$
    decomposition.
  \end{block}

\end{frame}



\begin{sagesilent}
  set_random_seed(723)
  A = random_matrix(ZZ, 2, 3)
  B = random_matrix(ZZ, 2, 3)
  S = A.T*A-B.T*B
  P, L, U = S.LU()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the $S=LU$ factorization
    \[
      \overset{S}{\footsage{S}}
      =
      \overset{L}{\footsage{L}}
      \overset{U}{\footsage{U}}
    \]\pause
    The pivots are $\sage{Set(U.diagonal())}$. \pause This means that $S$ is
    \pause \emph{indefinite}.
  \end{example}

\end{frame}



\subsection{The Cholesky Decomposition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Positive definite matrices can be factored as $S=LU$.
  \end{theorem}

  \pause
  \begin{theorem}[Cholesky Decomposition]
    Every positive definite matrix factors as $S=A^\intercal A$ where $A$ is
    upper triangular with real positive diagonal entries.
  \end{theorem}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{algorithm}[Cholesky Decomposition Algorithm]
    To determine if $S$ is positive definite and compute a Cholesky
    decomposition.
    \begin{description}
    \item<2->[Step 1] Factor $S=LU$.
      \begin{itemize}
      \item<3-> If a row swap is required, then $S$ is not positive definite.
      \item<4-> If a nonpositive pivot appears, then $S$ is not positive
        definite.
      \end{itemize}
    \item<5->[Step 2] Let $D$ be the diagonal matrix whose entries are the pivots in
      $U$.
    \item<6->[Step 3] Define $A=\sqrt{D}L^\intercal$.
    \end{description}
    \onslide<7->{This gives $S=A^\intercal A$ where $A$ is upper triangular with
      real positive diagonal entries.}
  \end{algorithm}

\end{frame}



\begin{sagesilent}
  S = matrix([(9, 3, 20), (3, 2, 3), (20, 3, 58)])
  from functools import partial
  elem = partial(elementary_matrix, S.nrows())
  l21 = -S[1, 0] / S[0, 0]
  l31 = -S[2, 0] / S[0, 0]
  E21 = elem(row1=1, row2=0, scale=l21)
  E31 = elem(row1=2, row2=0, scale=l31)
  S1 = E31*E21*S
  l32 = -S1[2, 1] / S1[1, 1]
  E32 = elem(row1=2, row2=1, scale=l32)
  U = E32*S1
  sqrtD = diagonal_matrix(map(sqrt, U.diagonal()))
  L = matrix([(1, 0, 0), (-l21, 1, 0), (-l31, -l32, 1)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the row-reductions
    \newcommand{\myStepA}{\arraycolsep=1pt\tiny
      \begin{array}{rcrcr}
        R_2 &+& (\sage{l21})\cdot R_1 &\to& R_2 \\
        R_3 &+& (\sage{l31})\cdot R_1 &\to& R_3
      \end{array}
    }
    \begin{align*}
      \onslide<2->{\overset{S}{\footsage{S}}}
      &\onslide<3->{\xrightarrow{\onslide<4->{\myStepA}}}\onslide<5->{\footsage{S1}} \\
      &\onslide<6->{\xrightarrow{\onslide<7->{R_3+(\sage{l32})\cdot R_2\to R_3}}}\onslide<8->{\overset{U}{\footsage{U}}}
    \end{align*}
    \onslide<9->{The pivots in $U$ are positive, so $S$ is positive definite. }\onslide<10->This gives the
    Cholesky decomposition $S=A^\intercal A$ where
    \[
      A
      = \overset{\sqrt{D}}{\sage{sqrtD}}
      \overset{L^\intercal}{\sage{L.T}}
      = \sage{sqrtD*L.T}
    \]
  \end{example}

\end{frame}




\begin{sagesilent}
  S = matrix([(1, 3, 11), (3, 9, 39), (11, 39, 60)])
  from functools import partial
  elem = partial(elementary_matrix, S.nrows())
  l21 = -S[1, 0] / S[0, 0]
  l31 = -S[2, 0] / S[0, 0]
  E21 = elem(row1=1, row2=0, scale=l21)
  E31 = elem(row1=2, row2=0, scale=l31)
  S1 = E31*E21*S
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the row-reductions
    \newcommand{\myStepA}{\arraycolsep=1pt\tiny
      \begin{array}{rcrcr}
        R_2 &+& (\sage{l21})\cdot R_1 &\to& R_2 \\
        R_3 &+& (\sage{l31})\cdot R_1 &\to& R_3
      \end{array}
    }
    \[
      \onslide<2->{\overset{S}{\sage{S}}}
      \onslide<3->{\xrightarrow{\onslide<4->{\myStepA}}}\onslide<5->{\sage{S1}}
    \]
    \onslide<6->{We need a row-swap to complete the factorization, so $S$ is not positive
    definite.}
  \end{example}

\end{frame}




\end{document}
