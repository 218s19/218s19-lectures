\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , shapes
  , shapes.geometric
  , arrows
  , fit
  , calc
  , positioning
  , automata
}
\usepackage{tikz-3dplot}

\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\title{The Algebra and Geometry of Vectors}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \tableofcontents[sections={1-4}]
    \column{.5\textwidth}
    \tableofcontents[sections={5-}]
  \end{columns}
\end{frame}


\section{Motivation}
\subsection{What We Know}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  So far, the vector operations we have learned are
  \gcenter{
    \mybold{scalar-vector muliplication} $c\cdot\vv{v}$ &&
    \mybold{vector addition} $\vv{v}+\vv{w}$
  }
  \onslide<2->{These operations are \emph{algebraic}}
  \newcommand{\myV}[2]{\left[\begin{array}{c}####1\\ \vdots\\ ####2\end{array}\right]}
  \begin{align*}
    \onslide<3->{c\cdot\myV{v_1}{v_n} &= \myV{c\cdot v_1}{c\cdot v_n}} & \onslide<4->{\myV{v_1}{v_n}+\myV{w_1}{w_n} &= \myV{v_1+w_1}{v_n+w_n}}
  \end{align*}
  % We use these operations to take \emph{linear combinations} of vectors
  % \[
  %   c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_n\vv*{v}{n}
  % \]

\end{frame}

\subsection{What We Want}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Can we interpret these operations \emph{geometrically}?

\end{frame}


\section{Vectors in $\RR^2$}
\subsection{Geometric Interpretation}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Geometrically, a vector in $\RR^2$ is represented by an \emph{arrow} in the
  $xy$-plane.\pause
  \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
      \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};

      \pause
      \draw[ultra thick, cyan, ->]
      (3, 3) -- (3, 2) node [below] {$\vv{a}$};

      \pause
      \draw[ultra thick, brown, ->]
      (3.5, 1) -- (3, 1) node [left] {$\vv{b}$};

      \pause
      \draw[ultra thick, blue, ->]
      (-3.5, 1) -- (-1, 2.5) node [above] {$\vv{v}$};

      \pause
      \draw[ultra thick, red, ->]
      (-3, -2.5) -- (-1, -1) node [above] {$\vv{w}$};

      \pause
      \draw[ultra thick, magenta, ->]
      (2.5, 2.5) -- (1, 2) node [left] {$\vv{x}$};

      \pause
      \draw[ultra thick, purple, ->]
      (3, -2.5) -- (1, 1) node [above] {$\vv{y}$};

      \pause
      \draw[ultra thick, orange, ->]
      (-1, 1) -- (2, -2) node [below] {$\vv{z}$};

    \end{tikzpicture}
  \]

\end{frame}



\subsection{Tails and Tips}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Every arrow emanates from a \emph{tail} and terminates at a \emph{tip}.
  \newcommand{\myxtic}[1]{
    \draw[ultra thick] (####1, 3pt) -- (####1, -3pt) node [below] {$####1$};
  }
  \newcommand{\myytic}[1]{
    \draw[ultra thick] (3pt, ####1) -- (-3pt, ####1) node [left] {$####1$};
  }
  \[
    \begin{tikzpicture}[line cap=round, line join=round]

      \onslide<2->{
        \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
        \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};

        \myxtic{-3}
        \myxtic{-2}
        \myxtic{-1}
        \myxtic{1}
        \myxtic{2}
        \myxtic{3}

        \myytic{-2}
        \myytic{-1}
        \myytic{1}
        \myytic{2}
      }

      \coordinate (P) at (-3, -1);
      \coordinate (Q) at (2, 2);

      \onslide<3->{
        \draw[ultra thick, blue, ->]
        (P) -- (Q) node [above, blue] {$\vv{v}\onslide<6->{=\vv{PQ}}$};;
      }

      \onslide<4->{
        \node at (P) {\textbullet};
        \node at (P) [below] {$\underset{\textnormal{``tail''}}{P(-3, -1)}$};
      }

      \onslide<5->{
        \node at (Q) {\textbullet};
        \node at (Q) [below right] {$\underset{\textnormal{``tip''}}{Q(2, 2)}$};
      }

      \coordinate (R) at (-2, -2);
      \coordinate (S) at (3, -1);


      \onslide<7->{
        \draw[ultra thick, red, ->]
        (R) -- (S) node [above] {$\vv{w}\onslide<10->{=\vv{RS}}$};
      }

      \onslide<8->{
        \node at (R) {\textbullet};
        \node at (R) [below] {$\underset{\textnormal{``tail''}}{R(-2, -2)}$};
      }

      \onslide<9->{
        \node at (S) {\textbullet};
        \node at (S) [below right] {$\underset{\textnormal{``tip''}}{S(3, -1)}$};
      }

    \end{tikzpicture}
  \]

\end{frame}


\begin{sagesilent}
  var('v1 v2')
  v = vector([v1, v2])
\end{sagesilent}
\subsection{Coordinates}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The \emph{coordinates} of $\vv{v}$ give the ``tip to tail'' displacement in
  the $x$- and $y$-directions.
  \[
    \begin{tikzpicture}[line cap=round, line join=round]

      \coordinate (P) at (0, 0);
      \coordinate (Q) at (6, 4);
      \coordinate (R) at (6, 0);

      \onslide<3->{
        \draw[thick, dashed, red]
        (P) -- (R) node [midway, below] {$v_1=\onslide<4->{x_1-x_0}$};


        \draw[thick, dashed, purple]
        (R) -- (Q) node [midway, right] {$v_2=\onslide<5->{y_1-y_0}$};
      }

      \onslide<2->{
        \draw[ultra thick, blue, ->]
        (P) -- (Q) node [midway, above, sloped] {$\vv{v}=\sage{v}$};

        \node at (P) {\textbullet};
        \node at (Q) {\textbullet};

        \node at (P) [below] {$(x_0, y_0)$};
        \node at (Q) [above] {$(x_1, y_1)$};
      }

    \end{tikzpicture}
  \]

\end{frame}






\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myxtic}[1]{
    \draw[ultra thick] (####1, 3pt) -- (####1, -3pt) node [below] {$####1$};
  }
  \newcommand{\myytic}[1]{
    \draw[ultra thick] (3pt, ####1) -- (-3pt, ####1) node [left] {$####1$};
  }
  \[
    \begin{tikzpicture}[line cap=round, line join=round]

      \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
      \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};

      \myxtic{-3}
      \myxtic{-2}
      \myxtic{-1}
      \myxtic{1}
      \myxtic{2}
      \myxtic{3}

      \myytic{-2}
      \myytic{-1}
      \myytic{1}
      \myytic{2}

      \coordinate (P) at (-3, -1);
      \coordinate (Q) at (2, 2);


      \draw[ultra thick, blue, ->]
      (P) -- (Q) node [above, blue] {\tiny$\vv{v}=\langle 2-(-3), 2-(-1)\rangle=\langle5, 3\rangle$};;

      \node at (P) {\textbullet};
      \node at (P) [below] {$P(-3, -1)$};


      \node at (Q) {\textbullet};
      \node at (Q) [below right] {$Q(2, 2)$};

      \coordinate (R) at (-2, -2);
      \coordinate (S) at (3, -1);


      \draw[ultra thick, red, ->]
      (R) -- (S) node [above] {\tiny$\vv{w}=\langle3-(-2), -1-(-2)\rangle=\langle5, 1\rangle$};


      \node at (R) {\textbullet};
      \node at (R) [below] {$R(-2, -2)$};


      \node at (S) {\textbullet};
      \node at (S) [below right] {$S(3, -1)$};

    \end{tikzpicture}
  \]

\end{frame}



\begin{sagesilent}
  P = vector([3, -1])
  Q = vector([1, -2])
  v = Q - P
  w = v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Two arrows define the same vector if they have the same coordinates.
  \newcommand{\myxtic}[1]{
    \draw[ultra thick] (####1, 3pt) -- (####1, -3pt) node [below] {$####1$};
  }
  \newcommand{\myytic}[1]{
    \draw[ultra thick] (3pt, ####1) -- (-3pt, ####1) node [left] {$####1$};
  }
  \[
    \begin{tikzpicture}[line cap=round, line join=round]

      \onslide<2->{
        \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
        \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};

        \myxtic{-3}
        \myxtic{-2}
        \myxtic{-1}
        \myxtic{1}
        \myxtic{2}
        \myxtic{3}

        \myytic{-2}
        \myytic{-1}
        \myytic{1}
        \myytic{2}
      }

      \coordinate (P) at (3, -1);
      \coordinate (Q) at (1, -2);

      \coordinate (R) at ($ (P) + (-4, 3) $);
      \coordinate (S) at ($ (Q) + (-4, 3) $);

      \onslide<3->{
        \draw[ultra thick, blue, ->]
        (P) -- (Q) node [midway, below, sloped] {$\vv{v}=\onslide<4->{\sage{v}}$};
      }

      \onslide<5->{
        \draw[ultra thick, red, ->]
        (R) -- (S) node [midway, below, sloped] {$\vv{w}=\onslide<6->{\sage{w}}$};
      }

      \onslide<7->{
        \node at (2, 2) {$\vv{v}=\vv{w}$};
      }

    \end{tikzpicture}
  \]

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Without context, it is convention to plot vectors using the origin as the
  tail.

  \pause
  \newcommand{\myxtic}[1]{
    \draw[ultra thick] (####1, 3pt) -- (####1, -3pt) node [below] {$####1$};
  }
  \newcommand{\myytic}[1]{
    \draw[ultra thick] (3pt, ####1) -- (-3pt, ####1) node [left] {$####1$};
  }
  \[
    \begin{tikzpicture}[line cap=round, line join=round]

      \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
      \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};

      \myxtic{-3}
      \myxtic{-2}
      \myxtic{-1}
      \myxtic{1}
      \myxtic{2}
      \myxtic{3}

      \myytic{-2}
      \myytic{-1}
      \myytic{1}
      \myytic{2}

      \coordinate (O) at (0, 0);

      \pause
      \coordinate (v) at (3, 2);
      \draw[ultra thick, blue, ->]
      (O) -- (v) node [above] {$\vv{v}=\langle 3, 2\rangle$};

      \pause
      \coordinate (w) at (-3, 1);
      \draw[ultra thick, red, ->]
      (O) -- (w) node [above] {$\vv{w}=\langle -3, 1\rangle$};

      \pause
      \coordinate (x) at (-2, -2);
      \draw[ultra thick, purple, ->]
      (O) -- (x) node [below] {$\vv{x}=\langle -2, -2\rangle$};

    \end{tikzpicture}
  \]

\end{frame}


\subsection{Length}
\begin{sagesilent}
  var('v1 v2')
  v = vector([v1, v2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The \emph{length} of a vector $\vv{v}=\sage{v}$ is
  $\norm{\vv{v}}=\onslide<4->{\sqrt{v_1^2+v_2^2}$.}
  \[
    \begin{tikzpicture}[line cap=round, line join=round]

      \coordinate (P) at (0, 0);
      \coordinate (Q) at (6, 4);
      \coordinate (R) at (6, 0);

      \onslide<3->{
        \draw[thick, dashed, red]
        (P) -- (R) node [midway, below] {$\abs{v_1}$};

        \draw[thick, dashed, purple]
        (R) -- (Q) node [midway, right] {$\abs{v_2}$};
      }

      \onslide<2->{
        \draw[ultra thick, blue, ->]
        (P) -- (Q) node [midway, above] {$\norm{\vv{v}}$};
      }

    \end{tikzpicture}
  \]

\end{frame}


\begin{sagesilent}
  set_random_seed(95792345457)
  r = random_vector(2)
  r1, r2 = r
  p = random_vector(2)
  p1, p2 = p
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The lengths of $\vv{r}=\sage{r}$ and $\vv{p}=\sage{p}$ are
    \begin{align*}
      \onslide<2->{\norm{\vv{r}} &= \sqrt{(\sage{r1})^2+(\sage{r2})^2}} & \onslide<5->{\norm{\vv{p}} &= \sqrt{(\sage{p1})^2+(\sage{p2})^2}} \\
      \onslide<3->{&= \sqrt{\sage{r1**2}+\sage{r2**2}}}   &          \onslide<6->{&= \sqrt{\sage{p1**2}+\sage{p2**2}}} \\
      \onslide<4->{&= \sage{r.norm()}}                    &          \onslide<7->{&= \sage{p.norm()}}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Other Terms for ``Length''}
    \gcenter{``norm'' && ``magnitude''}
  \end{block}

  \pause
  \begin{block}{Alternate Notation for $\norm{\vv{v}}$}
    Some people write $\abs{\vv{v}}$ instead of $\norm{\vv{v}}$.
  \end{block}

\end{frame}


\section{Vectors in $\RR^3$}
\subsection{Geometric Interpretation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  We think of vectors in $\RR^3$ as objects in \emph{$xyz$-space}.
  \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, scale=3/2, line join=round, line cap=round]
      \draw[ultra thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
      \draw[ultra thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
      \draw[ultra thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};

      \pause

      \draw[ultra thick] (1.5, 0, .1pt) -- (1.5, 0, -.1pt) node [below] {\tiny{$3$}};
      \draw[ultra thick] (0, 1, .1pt) -- (0, 1, -.1pt) node [below] {\tiny{$3$}};
      \draw[ultra thick] (0, .1pt, -3/2) -- (0, -.1pt, -3/2) node [left] {\tiny{$-4$}};
      \draw[thick, blue, dashed] (3/2, 0, 0) -- (3/2, 1, 0);
      \draw[thick, blue, dashed] (0, 1, 0) -- (3/2, 1, 0);
      \draw[thick, blue, dashed] (3/2, 1, 0) -- (3/2, 1, -3/2);
      \node[label={right:\tiny{$P(3,3,-4)$}}] at (3/2, 1, -3/2) {\textbullet};

      \pause

      \draw[ultra thick] (-2, 0, .1pt) -- (-2, 0, -.1pt) node [below] {\tiny{$-4$}};
      \draw[ultra thick] (0, 4/3, .1pt) -- (0, 4/3, -.1pt) node [below] {\tiny{$4$}};
      \draw[ultra thick] (0, .1pt, 5/3) -- (0, -.1pt, 5/3) node [left] {\tiny{$5$}};
      \draw[thick, blue, dashed] (-2, 0, 0) -- (-2, 4/3, 0);
      \draw[thick, blue, dashed] (0, 4/3, 0) -- (-2, 4/3, 0);
      \draw[thick, blue, dashed] (-2, 4/3, 0) -- (-2, 4/3, 5/3);
      \node[label={right:\tiny{$Q(-4,4,5)$}}] at (-2, 4/3, 5/3) {\textbullet};
    \end{tikzpicture}
  \]

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Geometrically, a vector in $\RR^3$ is represented by an \emph{arrow} in
  $xyz$-space.

  \begin{columns}[onlytextwidth]
    \column{.4\textwidth}
    \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[tdplot_main_coords, scale=1/4]
        \coordinate (P) at (11, 2, 0); \coordinate (Q) at (8,-4,6); \coordinate
        (R) at (-2,2,7); \coordinate (S) at (1,8,1);

        \draw[thick,->] (0,0,0) -- (12,0,0) node[anchor=north east]{$x$};
        \draw[thick,->] (0,0,0) -- (0,9,0) node[anchor=north west]{$y$};
        \draw[thick,->] (0,0,0) -- (0,0,9) node[anchor=south]{$z$};

        \filldraw (P) circle (8pt) node [below] {\tiny{$P(11,2,0)$}};
        \filldraw (Q) circle (8pt) node [below] {\tiny{$Q(8,-4,6)$}}; \filldraw
        (R) circle (8pt) node [above right] {\tiny{$R(-2,2,7)$}}; \filldraw (S)
        circle (8pt) node [above] {\tiny{$S(1,8,1)$}};

        \onslide<2->{\draw[ultra thick, blue, ->] (Q) -- (R) node [midway, above] {$\vv{v}$};}
        \onslide<4->{\draw[ultra thick, red, ->] (P) -- (R) node [near end, below] {$\vv{w}$};}
        \onslide<6->{\draw[ultra thick, purple, ->] (P) -- (S) node [midway, below] {$\vv{x}$};}
      \end{tikzpicture}
    \]
    \column{.6\textwidth}
    \begin{align*}
      \onslide<3->{\vv{v} &=
                            \left[\begin{array}{r}
                                    -2-8\\2-(-4)\\7-6
                                  \end{array}\right]
      = \left[\begin{array}{r}
                -10 \\
                6 \\
                1
              \end{array}\right] \\}
      \onslide<5->{\vv{w} &=
                            \left[\begin{array}{r}
                                    -2-11\\2-2\\7-0
                                  \end{array}\right]
      = \left[\begin{array}{r}
                -13 \\ 0 \\ 7
              \end{array}\right] \\}
      \onslide<7->{\vv{x} &=
                            \left[\begin{array}{r}
                                    1-11\\8-2\\1-0
                                  \end{array}\right]
      = \left[\begin{array}{r}
                -10 \\ 6 \\ 1
              \end{array}\right] \\}
    \end{align*}
  \end{columns}
  \onslide<8->{Note that $\vv{v}=\vv{x}$!}

\end{frame}



\subsection{Length}
\begin{sagesilent}
  var('v1 v2 v3')
  v = vector([v1, v2, v3])
  set_random_seed(49508477704972)
  z = random_vector(3, x=-10, y=10)
  z1, z2, z3 = z
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{length} of $\vv{v}=\sage{v}$ is
    $\norm{\vv{v}}=\sqrt{v_1^2+v_2^2+v_3^2}$.
  \end{definition}

  \pause
  \begin{example}
    The length of $\vv{z}=\sage{z}$ is
    \begin{align*}
      \norm{\vv{z}}
      &= \sqrt{(\sage{z1})^2+(\sage{z2})^2+(\sage{z3})^2} \\
      &= \sqrt{\sage{z1**2}+\sage{z2**2}+\sage{z3**2}} \\
      &= \sage{z.norm()}
    \end{align*}
  \end{example}

\end{frame}



\section{Vectors in $\RR^n$}
\subsection{``Geometric'' Interpretation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The ``visible'' geometry of $\RR^2$ and $\RR^3$ is used to \emph{define}
  geometry in higher dimensions.\pause
  \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \draw[ultra thick, cyan, ->]
      (3, 3) -- (3, 2) node [below] {$\vv{a}$};

      \draw[ultra thick, brown, ->]
      (3.5, 1) -- (3, 1) node [left] {$\vv{b}$};

      \draw[ultra thick, blue, ->]
      (-3.5, 1) -- (-1, 2.5) node [above] {$\vv{v}$};

      \draw[ultra thick, red, ->]
      (-3, -2.5) -- (-1, -1) node [above] {$\vv{w}$};

      \draw[ultra thick, magenta, ->]
      (2.5, 2.5) -- (1, 2) node [left] {$\vv{x}$};

      \draw[ultra thick, purple, ->]
      (3, -2.5) -- (1, 1) node [above] {$\vv{y}$};

      \draw[ultra thick, orange, ->]
      (-1, 1) -- (2, -2) node [below] {$\vv{z}$};

    \end{tikzpicture}
  \]\pause
  We think of a vector $\vv{v}\in\RR^n$ as an ``arrow'' in Euclidean $n$-space.
\end{frame}

\subsection{Length}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The \emph{length} of $\vv{v}=\langle v_1, v_2, \dotsc, v_n\rangle$ is
  \[
    \norm{\vv{v}}
    = \pause\sqrt{v_1^2+v_2^2+\dotsb+v_n^2}
  \]\pause
  Even if we can't ``see'' a vector, we can still compute its length.

\end{frame}


\begin{sagesilent}
  set_random_seed(497594)
  v = random_vector(5, x=-5, y=5)
  v1, v2, v3, v4, v5 = v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The length of $\vv{v}=\sage{v}$ is
    \begin{align*}
      \norm{\vv{v}}
      &= \sqrt{(\sage{v1})^2+(\sage{v2})^2+(\sage{v3})^2+(\sage{v4})^2+(\sage{v5})^2} \\
      &= \sqrt{\sage{v1**2}+\sage{v2**2}+\sage{v3**2}+\sage{v4**2}+\sage{v5**2}} \\
      &= \sage{v.norm()}
    \end{align*}
  \end{example}

  \pause
  \begin{example}
    Suppose $\vv{a}$ is an incidence vector of a graph on $40$ nodes and $77$
    arrows. Then $\vv{a}\in\pause\RR^{40}$ \pause and the length of $\vv{a}$ is
    $\norm{\vv{a}}=\pause\sqrt{2}$.
  \end{example}

\end{frame}


\section{Scalar-Vector Multiplication}
\subsection{Geometric Interpretation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Given a vector $\vv{v}\in\mathbb{R}^n$ and a scalar $c\in\mathbb{R}$, what
    ``should'' $c\cdot\vv{v}$ look like?
  \end{block}
  \newcommand{\myV}{
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0, 0);
      \coordinate (v) at (1, 1);

      \draw[ultra thick, blue, ->]
      (O) -- (v) node[midway, above, sloped] {$\vv{v}$};
    \end{tikzpicture}
  }
  \newcommand{\myVa}{
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0, 0);
      \coordinate (v) at (1, 1);

      \draw[ultra thick, red, ->]
      (O) -- ($ 2*(v) $) node[midway, above, sloped] {$2\cdot \vv{v}$};
    \end{tikzpicture}
  }
  \newcommand{\myVb}{
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0, 0);
      \coordinate (v) at (1, 1);

      \draw[ultra thick, purple, ->]
      (O) -- ($ .5*(v) $) node[midway, above, sloped] {$(\frac{1}{2})\cdot \vv{v}$};
    \end{tikzpicture}
  }
  \newcommand{\myVc}{
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0, 0);
      \coordinate (v) at (1, 1);

      \draw[ultra thick, orange, ->]
      (O) -- ($ -.5*(v) $) node[midway, above, sloped] {$(-\frac{1}{2})\cdot \vv{v}$};
    \end{tikzpicture}
  }
  \newcommand{\myVd}{
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0, 0);
      \coordinate (v) at (1, 1);

      \draw[ultra thick, cyan, ->]
      (O) -- ($ -1*(v) $) node[midway, above, sloped] {$-1\cdot \vv{v}$};
    \end{tikzpicture}
  }
  \gcenter{
    \pause\myV && \pause\myVa && \pause\myVb && \pause\myVc && \pause\myVd
  }

  % \[
  %   \begin{tikzpicture}[line cap=round, line join=round]
  %     \coordinate (O) at (0, 0);
  %     \coordinate (P) at (2, 1);
  %     \coordinate (S) at (1, 0);

  %     \onslide<2->{
  %     \draw[ultra thick, blue, ->]
  %     (O) -- (P)
  %     node[midway, above, sloped] {$\vv{v}$};
  %   }

  %     \onslide<3->{
  %     \draw[ultra thick, blue, ->]
  %     ($ (O) + 2*(S) $) -- ($ 2*(P) + 2*(S) $)
  %     node [midway, above, sloped] {$2\cdot\vv{v}$};
  %   }

  %     \onslide<4->{
  %     \draw[ultra thick, blue, ->]
  %     ($ (O) + 4*(S) $) -- ($ .5*(P) + 4*(S) $)
  %     node [midway, above, sloped] {$(\frac{1}{2})\cdot\vv{v}$};
  %   }


  %     \onslide<5->{
  %     \draw[ultra thick, blue, <-]
  %     ($ (O) + 6*(S) $) -- ($ (P) + 6*(S)$)
  %     node [midway, above, sloped] {$-\vv{v}$};
  %   }

  %     \onslide<6->{
  %     \draw[ultra thick, blue, <-]
  %     ($ (O) + 8*(S) $) -- ($ .5*(P) + 8*(S) $)
  %     node [midway, above, sloped] {$(-\frac{1}{2})\cdot\vv{v}$};
  %   }

  %   \end{tikzpicture}
  % \]

  \begin{definition}<7->[Geometric] The \emph{scalar-vector product} of $c$ and
    $\vv{v}$ is the vector $c\cdot\vv{v}$ whose magnitude is
    $\abs{c}\cdot\norm{\vv{v}}$ and whose direction is either the direction of
    $\vv{v}$ if $c>0$ or the opposite direction of $\vv{v}$ if $c<0$.
  \end{definition}

\end{frame}


\subsection{Algebraic Comparison}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  This ``geometric'' interpretation coincides with our ``algebraic'' definition.
  \begin{align*}
    \norm{c\cdot\vv{v}}
    \onslide<+->{&= \norm{\langle c\cdot v_1,c\cdot v_2,\dotsc,c\cdot v_n\rangle} \\}
    \onslide<+->{&= \sqrt{(c\cdot v_1)^2+(c\cdot v_2)^2+\dotsb+(c\cdot v_n)^2} \\}
    \onslide<+->{&= \sqrt{c^2\cdot v_1^2+c^2\cdot v_2^2+\dotsb+c^2\cdot v_n^2} \\}
    \onslide<+->{&= \sqrt{c^2\cdot(v_1^2+v_2^2+\dotsb+v_n^2)} \\}
    \onslide<+->{&= \sqrt{c^2}\cdot\sqrt{v_1^2+v_2^2+\dotsb+v_n^2} \\}
    \onslide<+->{&= \abs{c}\cdot\norm{\vv{v}}}
  \end{align*}

\end{frame}


\subsection{Unit Vectors}
\begin{sagesilent}
  v = vector([1, -1, -1, 1])
  u = v.normalized()
  v1, v2, v3, v4 = v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{unit vector} is a vector with length one.
  \end{definition}

  \pause
  \begin{example}
    Let $\vv{u}=\sage{u}$. Then
    \begin{align*}
      \norm{\vv{u}}
      \onslide<+->{&= \norm{(\sfrac{1}{2})\cdot\sage{v}} \\}
      \onslide<+->{&= \abs{\sfrac{1}{2}}\cdot\norm{\sage{v}} \\}
      \onslide<+->{&= (\sfrac{1}{2})\cdot\sqrt{(\sage{v1})^2+(\sage{v2})^2+(\sage{v3})^2+(\sage{v4})^2} \\}
      \onslide<+->{&= (\sfrac{1}{2})\cdot\sqrt{\sage{v1**2}+\sage{v2**2}+\sage{v3**2}+\sage{v4**2}} \\}
      \onslide<+->{&= (\sfrac{1}{2})\cdot\sqrt{\sage{v.norm()**2}} \\}
      \onslide<+->{&= (\sfrac{1}{2})\cdot\sage{v.norm()} \\}
      \onslide<+->{&= \sage{u.norm()}}
    \end{align*}

  \end{example}

\end{frame}


\begin{sagesilent}
  e1, e2, e3 = identity_matrix(3).columns()
  var('v1 v2 v3')
  v = vector([v1, v2, v3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vectors
    \begin{align*}
      \vv*{e}{1} &= \sage{e1} & \vv*{e}{2} &= \sage{e2} & \vv*{e}{3} &= \sage{e3}
    \end{align*}
    Each $\vv*{e}{1}$, $\vv*{e}{2}$, and $\vv*{e}{3}$ is a unit vector.
  \end{example}

  \pause
  \begin{definition}
    These vectors form the \emph{standard basis} of $\RR^3$.
  \end{definition}

  \pause
  \begin{block}{Important}
    Every vector $\vv{v}=\sage{v}$ satisfies
    \[
      \vv{v}
      = v_1\cdot\vv*{e}{1}+v_2\cdot\vv*{e}{2}+v_3\cdot\vv*{e}{3}
    \]\pause
    Every vector is a \pause\emph{linear combination} of the standard basis!
  \end{block}

\end{frame}


\subsection{Normalization}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.7\textwidth}
    \begin{definition}
      The \emph{normalization} of $\vv{v}$ is
      $\widehat{v}=(\frac{1}{\norm{\vv{v}}})\cdot\vv{v}$.
    \end{definition}

    \column{.3\textwidth}
    \[
      \begin{tikzpicture}[line cap=round, line join=round, scale=3/5]
        \coordinate (O) at (0,0);
        \coordinate (P) at (4,2);
        \coordinate (Q) at (2,1);

        \draw[ultra thick, blue, ->]
        (O) -- (P) node[above] {$\vv{v}$};

        \onslide<2->{
          \draw[ultra thick, purple, ->]
          (O) -- (Q) node[above] {$\widehat{v}$};
        }
      \end{tikzpicture}
    \]
  \end{columns}
  \onslide<3->{Note that}
  \[
    \onslide<3->{\norm{\widehat{v}} =}
    \onslide<4->{\norm*{\frac{1}{\norm{\vv{v}}}\cdot\vv{v}} =}
    \onslide<5->{\abs*{\frac{1}{\norm{\vv{v}}}}\cdot\norm{\vv{v}} =}
    \onslide<6->{\frac{1}{\norm{\vv{v}}}\cdot\norm{\vv{v}} =}
    \onslide<7->{1}
  \]
  \onslide<8->{The normalization $\widehat{v}$ of $\vv{v}$ is always a unit
    vector!}


\end{frame}


\section{Vector Addition}
\subsection{Geometric Interpretation}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  To interpret $\vv{v}+\vv{w}$ geometrically, start by forming a
  parallelogram. \onslide<5->{The sum $\vv{v}+\vv{w}$ is the diagonal of the
    parallelogram, obtained by first transversing $\vv{v}$ and then transversing
    $\vv{w}$.}
  \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0,0);
      \coordinate (P) at (5,1);
      \coordinate (Q) at (1,3);

      \onslide<2->{
        \draw[ultra thick, blue, ->]
        (O) -- (P) node[midway, below] {$\vv{v}$};

        \draw[ultra thick, red, ->]
        (O) -- (Q) node[midway, left] {$\vv{w}$};
      }

      \onslide<3->{
        \draw[ultra thick, blue, ->]
        (Q) -- ($ (P) + (Q) $) node[midway, above] {$\vv{v}$};
      }

      \onslide<4->{
        \draw[ultra thick, red, ->]
        (P) -- ($ (P) + (Q) $) node[midway, right] {$\vv{w}$};
      }

      \onslide<6->{
        \draw[ultra thick, violet, ->]%shorten >=8pt,
        (O) -- ($ (P) + (Q) $) node[midway, above, sloped] {$\vv{v}+\vv{w}$};
      }
    \end{tikzpicture}
  \]

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  This geometric interpretation allows us to construct \emph{vector diagrams}.
  \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0,0);
      \coordinate (P) at (3,4);
      \coordinate (Q) at (6, -1);
      \coordinate (R) at (-2, 4);


      \draw[ultra thick, blue, ->]
      (O) -- (P) node[midway, above, fill=white] {$\vv{v}$};

      \draw[ultra thick, red, ->]
      (Q) -- (P) node[midway, above, fill=white] {$\vv{w}$};

      \draw[ultra thick, purple, ->]
      (O) -- (R) node[midway, below, fill=white] {$\vv{x}$};

      \draw[ultra thick, orange, ->]
      (R) -- (P) node[midway, above, sloped] {$\vv{y}=\onslide<2->{\vv{v}-\vv{x}}$};

      \draw[ultra thick, brown, ->]
      (Q) -- (O) node[midway, below, sloped] {$\vv{z}=\onslide<3->{\vv{w}-\vv{v}}$};

    \end{tikzpicture}
  \]

\end{frame}

\subsection{Properties}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties of Vector Arithmetic}
    Vector arithmetic obeys the following laws.
    \begin{description}[multiplicitive associativity]
    \item<+->[additive commutativity] $\vv{v}+\vv{w}=\vv{w}+\vv{v}$
    \item<+->[additive associativity] $(\vv{v}+\vv{w})+\vv{x}=\vv{v}+(\vv{w}+\vv{x})$
    \item<+->[multiplicitive associativity] $(c\cdot d)\cdot\vv{v}=c\cdot(d\cdot\vv{v})$
    \item<+->[scalar distribution] $c\cdot(\vv{v}+\vv{w})=c\cdot\vv{v}+c\cdot\vv{w}$
    \item<+->[vector distribution] $(c+d)\cdot\vv{v}=c\cdot\vv{v}+d\cdot\vv{v}$
    \item<+->[additive identity] the \emph{zero vector}
      $\vv{O}=\langle0, 0, \dotsc, 0\rangle$ satisfies the equation
      $\vv{O}+\vv{v}=\vv{v}$
    \end{description}
  \end{block}

  \onslide<+->
  \begin{block}{Note}
    The zero vector $\vv{O}$ is the only vector whose length is zero
    $\norm{\vv{O}}=0$.
  \end{block}

\end{frame}


\section{The Dot Product}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What does it mean to multiply two vectors?
  \end{block}

  \pause
  \begin{block}{Answer}
    There is no correct answer. There are several useful ways to multiply
    vectors.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{dot product} of $\vv{v}=\langle v_1,v_2,\dotsc,v_n\rangle$ and
    $\vv{w}=\langle w_1,w_2,\dotsc,w_n\rangle$ is defined as
    \[
      \vv{v}\cdot\vv{w}
      = v_1\cdot w_1+v_2\cdot w_2+\dotsb+v_n\cdot w_n
    \]
  \end{definition}

  \pause
  \begin{block}{Note}
    The dot product of two vectors is a scalar, \emph{not} a vector.
  \end{block}

  \pause
  \begin{block}{Note}
    The dot product $\vv{v}\cdot\vv{w}$ is only defined if $\vv{v}$ and
    $\vv{w}$ have the \emph{same number of coordinates}.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(3)
  v = random_vector(3)
  w = random_vector(3)
  while len(set(v.list()+w.list())) != len(v)+len(w): v = random_vector(3); w = random_vector(3)
  v1, v2, v3 = v
  w1, w2, w3 = w
  vb = random_matrix(ZZ, 3, 1)
  wb = random_matrix(ZZ, 4, 1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv{v}=\sage{v}$ and $\vv{w}=\sage{w}$. \onslide<+->{Then}
    \begin{align*}
      \onslide<+->{\vv{v}\cdot\vv{w}
      &=} \onslide<+->{\sage{v}\cdot\sage{w} \\
      &=} \onslide<+->{(\sage{v1})(\sage{w1})+(\sage{v2})(\sage{w2})+(\sage{v3})(\sage{w3}) \\
      &=} \onslide<+->{(\sage{v1*w1})+(\sage{v2*w2})+(\sage{v3*w3}) \\
      &=} \onslide<+->{\sage{v*w}}
    \end{align*}
  \end{example}

  \onslide<+->
  \begin{example}
    The dot product of $\sage{vb}$ and $\sage{wb}$ is not defined!
  \end{example}

\end{frame}


\subsection{Properties}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algebraic Properties of the Dot Product}
    The dot product obeys the following laws.
    \begin{description}[commutative]
    \item<+->[commutative] $\vv{v}\cdot\vv{w}=\vv{w}\cdot\vv{v}$
    \item<+->[distributive] $\vv{v}\cdot(\vv{w}+\vv{x})=\vv{v}\cdot\vv{w}+\vv{v}\cdot\vv{x}$
    \item<+->[associative] $c\cdot(\vv{v}\cdot\vv{w})=(c\cdot\vv{v})\cdot\vv{w}$
    \end{description}
  \end{block}
\end{frame}



\subsection{Lengths}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    For $\vv{v}=\langle v_1, v_2, \dotsc, v_n\rangle$, we have
    \begin{align*}
      \onslide<2->{\vv{v}\cdot\vv{v}
      &=} \onslide<3->{v_1\cdot v_1+v_2\cdot v_2+\dotsb+v_n\cdot v_n \\
      &=} \onslide<4->{v_1^2+v_2^2+\dotsb+v_n^2 \\
      &=} \onslide<5->{\norm{\vv{v}}^2}
    \end{align*}
    \onslide<6->{The dot product can be used to compute lengths!}
  \end{block}


\end{frame}


\subsection{Angles}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      How can we measure the angle $\theta$ between two vectors $\vv{v}$ and
      $\vv{w}$?
    \end{block}
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[line cap=round, line join=round, scale=3/4]
        \coordinate (O) at (0,0); \coordinate (P) at (5,0); \coordinate (Q) at
        (2,3);

        \draw[ultra thick, blue, ->] (O) -- (P) node[midway, below] {$\vv{v}$};
        \draw[ultra thick, red, ->] (O) -- (Q) node[midway, left] {$\vv{w}$};

        \draw[<->, thick] (1,0) arc (0:56.31:1); \node (theta) at (7/6,3/6)
        {$\theta$};

        \onslide<3->{
          \draw[ultra thick, violet, ->] (Q) -- (P) node[midway, right]
          {$\vv{v}-\vv{w}$};}
      \end{tikzpicture}
    \]
  \end{columns}

  \onslide<2->{
    \begin{block}{Answer}
      Form a triangle.} \onslide<4->{Measure $\norm{\vv{v}-\vv{w}}^2$ in two ways.}
    \begin{description}[law of cosines]
    \item<5->[law of cosines]
      $\norm{\vv{v}-\vv{w}}^2
      = \norm{\vv{v}}^2+\norm{\vv{w}}^2-2\,\norm{\vv{v}}\cdot\norm{\vv{w}}\cos\theta$
    \item<6->[dot product]
      $\norm{\vv{v}-\vv{w}}^2
      = \norm{\vv{v}}^2+\norm{\vv{w}}^2-2\,\vv{v}\cdot\vv{w}$
    \end{description}
  \end{block}

  \onslide<7->{
    \begin{theorem}
      $\vv{v}\cdot\vv{w}=\norm{\vv{v}}\cdot\norm{\vv{w}}\cos\theta$
    \end{theorem}}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\vv{v}\cdot\vv{w}=\norm{\vv{v}}\cdot\norm{\vv{w}}\cos\theta$
  \end{theorem}

  \pause
  \begin{corollary}[The Cauchy-Schwarz Inequality]
    $\abs{\vv{v}\cdot\vv{w}}\leq\norm{\vv{v}}\cdot\norm{\vv{w}}$
  \end{corollary}

  \pause
  \begin{example}
    Let $\theta$ be the angle between $\vv{v}=\langle1,2,3\rangle$ and
    $\vv{w}=\langle1, 1, 1\rangle$. Compute $\cos\theta$.
    \pause
    \[
      \cos\theta
      = \frac{\vv{v}\cdot\vv{w}}{\norm{\vv{v}}\cdot\norm{\vv{w}}}
      = \frac{(1)(1)+(2)(1)+(3)(1)}{\sqrt{1^2+2^2+3^2}\cdot\sqrt{1^2+1^2+1^2}}
      = \frac{6}{\sqrt{14}\cdot\sqrt{3}}
    \]
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\theta$ be the angle between two vectors $\vv{v}\neq\vv{O}$ and
    $\vv{w}\neq\vv{O}$. Then
    \begin{description}
    \item<2->[$\vv{v}\cdot\vv{w}>0$] means $\theta$ is \onslide<3->{\emph{acute} $\left(0\leq\theta<\frac{\pi}{2}\right)$}
    \item<4->[$\vv{v}\cdot\vv{w}<0$] means $\theta$ is \onslide<5->{\emph{obtuse} $\left(\frac{\pi}{2}<\theta\leq\pi\right)$}
    \item<6->[$\vv{v}\cdot\vv{w}=0$] means $\theta$ is \onslide<7->{\emph{right} $\left(\theta=\frac{\pi}{2}\right)$}
    \end{description}
  \end{theorem}

  \onslide<8->
    \begin{definition}
      $\vv{v}$ and $\vv{w}$ are \emph{orthogonal} if $\vv{v}\cdot\vv{w}=0$
    \end{definition}

  \onslide<9->
    \begin{example}
      Are $\vv{v}=\langle1,\sqrt{2},1,0\rangle$ and
      $\vv{w}=\langle 1, -\sqrt{2},1,1\rangle$ orthogonal? \onslide<10->Yes,
      since
      \[
        \vv{v}\cdot\vv{w}
        = (1)(1)+(\sqrt{2})(-\sqrt{2})+(1)(1)+(0)(1)
        = 1-2+1+0
        = 0
      \]
    \end{example}

\end{frame}




\begin{sagesilent}
  v = vector([1, -3, 8])
  v1, v2, v3 = v
  var('w1 w2 w3')
  w = vector([w1, w2, w3])
  wc = matrix.column(w)
  wca = matrix.column([(-v2*w2-v3*w3)/v1, w2, w3])
  wc1 = matrix.column([-v2/v1, 1, 0])
  wc2 = matrix.column([-v3/v1, 0, 1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    Describe all vectors orthogonal to $\vv{v}=\sage{v}$.
  \end{block}
  \pause
  The vectors orthogonal to $\vv{v}$ are the vectors $\vv{w}=\sage{w}$
  satisfying $\vv{v}\cdot\vv{w}=\pause0$. \pause This gives the equation
  \[
    0
    = \vv{v}\cdot\vv{w}
    = \sage{v}\cdot\sage{w}
    = \sage{v*w}
  \]\pause
  Solving this equation for $w_1$ gives
  $w_1=\pause\sage{(-v2*w2-v3*w3)/v1}$. \pause The vectors orthogonal to
  $\vv{v}$ are thus given by
  \[
    \vv{w}
    = \sage{wc}
    = \pause\sage{wca}
    = \pause w_2\sage{wc1}+w_3\sage{wc2}
  \]\pause
  where $w_2$ and $w_3$ can be chosen ``freely.'' \pause The vectors orthogonal
  to $\vv{v}$ are the \emph{linear combinations} of $\sage{vector([3, 1, 0])}$
  and $\sage{vector([-8, 0, 1])}$.



\end{frame}


\end{document}
