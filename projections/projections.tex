\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\PassOptionsToPackage{usenames, dvipsnames, svgnames}{xcolor}
\usepackage{tikz}
\usetikzlibrary{
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , positioning
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%
\DeclarePairedDelimiter\pair{(}{)}%

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\codim}{codim}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Projections}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-3}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={4-}]
  % \end{columns}
\end{frame}


\section{Preliminaries}
\subsection{Motivation}

\begin{sagesilent}
  var('b1 b2 b3')
  b = vector([b1, b2, b3])
  p = vector([0, 0, b3])
  P = matrix([(0, 0, 0), (0, 0, 0), (0, 0, 1)])
  bc = matrix.column(b)
  pc = matrix.column(p)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      What is the \emph{projection} $\vv{p}$ of $\vv{b}=\sage{b}$ onto the
      $z$-axis?
    \end{block}
    \column{.5\textwidth}
    \[
      \tdplotsetmaincoords{70}{110}
      \scalebox{.75}{%
        \begin{tikzpicture}[tdplot_main_coords, scale=3/2, line join=round, line cap=round]
          \coordinate (O) at (0, 0, 0);

          \coordinate (e1) at (3, 0, 0);
          \coordinate (e2) at (0, 2, 0);
          \coordinate (e3) at (0, 0, 2);

          \draw[ultra thick,->] (O) -- (e1) node[anchor=north east]{$x$};
          \draw[ultra thick,->] (O) -- (e2) node[anchor=north west]{$y$};
          \draw[ultra thick,->] (O) -- (e3) node[anchor=south]{$z$};

          \pgfmathsetmacro{\mybx}{1}
          \pgfmathsetmacro{\myby}{2}
          \pgfmathsetmacro{\mybz}{1.5}
          \coordinate (b) at (\mybx, \myby, \mybz);
          \coordinate (p) at (0, 0, \mybz);

          \onslide<3->{
            \draw[ultra thick]
            ($ (p)+.1*(b)-.1*(p) $)
            -- ($ (p)+.1*(b)-.1*(p)+.1*(e3) $)
            -- ($ (p)+.1*(e3) $);

            \draw[ultra thick, red, dashed] (b) -- (p);
          }

          \draw[ultra thick, blue, ->] (O) -- (b) node[right] {$\vv{b}=\sage{b}$};

          \onslide<4->{
            \draw[ultra thick, violet, ->] (O) -- (p) node[left] {$\onslide<5->{\vv{p}=\sage{p}}$};
          }
        \end{tikzpicture}
      }
    \]
  \end{columns}
  \begin{block}{\onslide<2->{Answer}}
    \onslide<2->{Draw an orthogonal ray from the tip of $\vv{b}$ to the
      $z$-axis. }%
    \onslide<4->{This gives the projection $\vv{p}=\onslide<5->{\sage{p}$.} }%
    \onslide<6->{Note that}
    \newcommand{\myProp}{%
      \begin{array}{rcl}
        \onslide<7->{\Null(P)} &\onslide<7->{=}& \onslide<8->{xy\textnormal{-plane}} \\
        \onslide<9->{\Col(P)} &\onslide<9->{=}& \onslide<10->{z\textnormal{-axis}}
      \end{array}
    }
    \begin{align*}
      \onslide<6->{\underset{P}{\sage{P}}\underset{\vv{b}}{\sage{bc}}=\underset{\vv{p}}{\sage{pc}}} && \myProp
    \end{align*}

  \end{block}

\end{frame}




\begin{sagesilent}
  var('b1 b2 b3')
  b = vector([b1, b2, b3])
  p = vector([b1, b2, 0])
  P = matrix([(1, 0, 0), (0, 1, 0), (0, 0, 0)])
  bc = matrix.column(b)
  pc = matrix.column(p)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      What is the \emph{projection} $\vv{p}$ of $\vv{b}=\sage{b}$ onto the
      $xy$-plane?
    \end{block}
    \column{.5\textwidth}
    \[
      \tdplotsetmaincoords{70}{110}
      \scalebox{.75}{%
        \begin{tikzpicture}[tdplot_main_coords, scale=3/2, line join=round, line cap=round]
          \coordinate (O) at (0, 0, 0);

          \coordinate (e1) at (3, 0, 0);
          \coordinate (e2) at (0, 2, 0);
          \coordinate (e3) at (0, 0, 2);

          \draw[ultra thick,->] (O) -- (e1) node[anchor=north east]{$x$};
          \draw[ultra thick,->] (O) -- (e2) node[anchor=north west]{$y$};
          \draw[ultra thick,->] (O) -- (e3) node[anchor=south]{$z$};

          \pgfmathsetmacro{\mybx}{1}
          \pgfmathsetmacro{\myby}{2}
          \pgfmathsetmacro{\mybz}{1.5}
          \coordinate (b) at (\mybx, \myby, \mybz);
          \coordinate (p) at (\mybx, \myby, 0);

          \onslide<3->{
            \draw[ultra thick]
            ($ (p)+.1*(b)-.1*(p) $)
            -- ($ 1.1*(p)+.1*(e3) $)
            -- ($ 1.1*(p) $);

            \draw[ultra thick, red, dashed] (b) -- (p);
          }

          \draw[ultra thick, blue, ->] (O) -- (b) node[above] {$\vv{b}=\sage{b}$};

          \onslide<4->{
            \draw[ultra thick, violet, ->] (O) -- (p) node[below] {$\onslide<5->{\vv{p}=\sage{p}}$};
          }
        \end{tikzpicture}
      }
    \]
  \end{columns}
  \begin{block}{\onslide<2->{Answer}}
    \onslide<2->{Draw an orthogonal ray from the tip of $\vv{b}$ to the
      $xy$-plane. }%
    \onslide<4->{This gives the projection $\vv{p}=\onslide<5->{\sage{p}$.} }%
    \onslide<6->{Note that }%
    \newcommand{\myProp}{%
      \begin{array}{rcl}
        \onslide<7->{\Null(P)} &\onslide<7->{=}& \onslide<8->{z\textnormal{-axis}} \\
        \onslide<9->{\Col(P)} &\onslide<9->{=}& \onslide<10->{xy\textnormal{-plane}}
      \end{array}
    }
    \begin{align*}
      \onslide<6->{\underset{P}{\sage{P}}\underset{\vv{b}}{\sage{bc}}=\underset{\vv{p}}{\sage{pc}}} && \myProp
    \end{align*}

  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Suppose $V\subset\mathbb{R}^n$ is a vector subspace.

  \pause
  \begin{block}{Question 1}
    What is the projection of a given $\vv{b}\in\mathbb{R}^n$ onto $V$?
  \end{block}

  \pause
  \begin{block}{Question 2}
    Is there a matrix $P$ that projects any given $\vv{b}\in\mathbb{R}^n$ onto
    $V$?
  \end{block}

\end{frame}


\subsection{The Gramian}

\begin{sagesilent}
  set_random_seed(459)
  A = random_matrix(ZZ, 2, 3, algorithm='echelonizable', rank=2, upper_bound=3)
  G = A.T*A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{Gramian} of $A$ is $G=A^\intercal A$.
  \end{definition}

  \pause
  \begin{example}
    Consider the matrix $A$ given by
    \[
      A=\sage{A}
    \]\pause
    The Gramian of $A$ is
    \[
      G
      = \pause\sage{A.T}\sage{A}
      = \pause\sage{G}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties of the Gramian}
    The Gramian $G=A^\intercal A$ of $A$ has the following properties.
    \begin{description}[<+->][common rank]
    \item[square] $G=A^\intercal A$ is $n\times n$ if $A$ is $m\times n$
    \item[symmetric] $G^\intercal=(A^\intercal A)^\intercal=A^\intercal(A^\intercal)^\intercal=A^\intercal A=G$
    \item[common rank] $\rank(G)=\rank(A^\intercal A)=\rank(A)$
    \end{description}
  \end{block}


  \onslide<+->
  \begin{block}{Invertibility of the Gramian}
    Suppose that $A$ is $m\times n$. Then the Gramian $G=A^\intercal A$ is
    \onslide<+-> $n\times n$ \onslide<+->and
    $\rank(G)=\onslide<+->\rank(A)$. \onslide<+-> This means that $G$ is
    invertible if and only if $\rank(A)=\onslide<+->n$. \onslide<+-> So
    $(A^\intercal A)^{-1}$ exists if and only if $A$ has \emph{full column
      rank}.
  \end{block}

\end{frame}



\section{Projection Onto $\Col(A)$}
\subsection{The Projection Matrix}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{columns}[onlytextwidth]
    \column{.4\textwidth}
    \begin{block}{Question}
      What matrix $P$ projects any vector $\vv{b}$ onto $\Col(A)$?
    \end{block}
    \column{.6\textwidth}
    \[
      \scalebox{.6}{%
        \begin{tikzpicture}[line join=round, line cap=round]
          \coordinate (O) at (0, 0, 0);

          \pgfmathsetmacro{\mysa}{5}
          \pgfmathsetmacro{\mysb}{2}

          \pgfmathsetmacro{\myp}{7/10}
          \pgfmathsetmacro{\myh}{3}


          \coordinate (e1) at (1, 0, 0);
          \coordinate (e2) at (0, 0, 1);
          \coordinate (e3) at (0, 1, 0);

          \coordinate[label={}] (se) at ($ \mysa*(e1)+\mysb*(e2) $); %(\mysa, 0, \mysa);
          \coordinate[label={}] (sw) at ($ -\mysb*(e1)+\mysb*(e2) $); %(-\mysa, 0, \mysa);
          \coordinate[label={}] (nw) at ($ -\mysb*(e1)-\mysa*(e2) $); %(-\mysa, 0, -\mysa);
          \coordinate[label={}] (ne) at ($ \mysa*(e1)-\mysa*(e2) $); %(\mysa, 0, -\mysa);


          \coordinate (p) at ($ (O)!\myp!(ne) $);
          \coordinate (b) at ($ (p)+\myh*(e3) $);
          % \coordinate (v) at

          \draw[ultra thick, draw=black, fill=Aquamarine!20]
          (se) -- (sw) -- (nw) -- (ne) -- cycle;

          \draw[ultra thick] (sw) -- (se)
          node [above, sloped, near end] {$\Col(A)$};



          \tikzset{
            c/.style={every coordinate/.try}
          }
          \begin{scope}[every coordinate/.style={shift={(p)}}]

            \pgfmathsetmacro{\myorth}{1/5}

            \coordinate (ortht) at ([c]$ \myorth*(e3) $);
            \coordinate (orthb) at ([c]$ \myorth*(e1) $);
            \coordinate (orthm) at ([c]$ \myorth*(e1)+\myorth*(e3) $);

            \draw[ultra thick]  (ortht) -- (orthm) -- (orthb);


          \end{scope}

          \onslide<5->{
            \draw[ultra thick, red, ->] (p) -- (b)
            node[midway, right] {$\vv{b}-A\widehat{x}$};
          }

          \draw[ultra thick, dashed, red] (p) -- (b);

          \draw[ultra thick, blue, ->] (O) -- (b)
          node[above] {$\vv{b}$};

          \draw[ultra thick, violet, ->] (O) -- (p)
          node[near end, sloped, below]
          {$P\vv{b}\onslide<3->{=A\widehat{x}}$};

        \end{tikzpicture}
      }
    \]
  \end{columns}
  \pause
  \begin{block}{Answer}
    \onslide<+->{Note that $P\vv{b}=A\widehat{x}$ because
      $P\vv{b}\in\onslide<+->{\Col(A)$. }} \onslide<+->{Then}
    \begin{align*}
      \onslide<+->{\vv{b}-A\widehat{x}\perp\Col(A)} && \onslide<+->{\vv{b}-A\widehat{x}\in\LNull(A)} && \onslide<+->{A^\intercal (\vv{b}-A\widehat{x})=\vv{O}} \\
      \onslide<+->{A^\intercal A\widehat{x}=A^\intercal\vv{b}} && \onslide<+->{\widehat{x}=(A^\intercal A)^{-1}A^\intercal\vv{b}} && \onslide<+->{A\widehat{x}=A(A^\intercal A)^{-1}A^\intercal\vv{b}}
    \end{align*}
    \onslide<+->{This means that $P\vv{b}=A(A^\intercal A)^{-1}A^\intercal\vv{b}$. }%
    \onslide<+->{Hence $P=A(A^\intercal A)^{-1}A^\intercal$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Suppose that $A$ has full column rank. The \emph{projection matrix} onto
    $\Col(A)$ is $P=A(A^\intercal A)^{-1}A^\intercal$.
  \end{definition}

  \pause
  \begin{block}{Note}
    $(A^\intercal A)^{-1}$ exists because \emph{$A$ has \pause full column rank}.
  \end{block}

  \pause
  \begin{block}{Note}
    $(A^\intercal A)^{-1}\neq A^{-1}(A^\intercal)^{-1}$ because \emph{$A$ might
      be \pause rectangular}.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The formula $P = A(A^\intercal A)^{-1} A^\intercal$ defines $P$ as
    projection onto $\Col(A)$ \pause as long as $A$ \emph{has \pause full column
      rank}.
  \end{block}

  \pause
  \begin{block}{If $A$ does not have full column rank}
    We start by finding a basis $\Set{\vv*{a}{1},\dotsc, \vv*{a}{d}}$ of
    $\Col(A)$. \pause Projection onto $\Col(A)$ is given by
    \[
      P = X(X^\intercal X)^{-1} X^\intercal
    \]
    \pause where
    $X=\pause \begin{bmatrix}\vv*{a}{1} & \dotsb & \vv*{a}{d}\end{bmatrix}$.
  \end{block}

\end{frame}


\subsection{Properties}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties of Projection Matrices}
    The projection matrix $P$ onto $\Col(A)$ has the following properties
    \begin{description}[<+->]
    \item[symmetric] $P^\intercal=P$
    \item[idempotent] $P^2=P$
    \item[$\vv{b}\in\Col(A)$] means $P\vv{b}=\vv{b}$
    \item[error] minimizing $\norm{\vv{b}-\vv{v}}$ with
      $\vv{v}\in\Col(A)$ is accomplished with $\vv{v}=P\vv{b}$
    \item[$\Col(P)$] $=\Col(A)$
    \item[$\Null(P)$] $=\LNull(A)$
    \end{description}
  \end{block}

\end{frame}



\section{Examples}

\subsection{$\rank(A)=1$}


\begin{sagesilent}
  a = vector([3, 1, -4])
  b = vector([1, -2, -1])
  A = matrix.column(a)
  bc = matrix.column(b)
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}
    \begin{block}{Problem}
      \onslide<1->{Consider $\vv{a}=\sage{a}$ }%
      \onslide<3->{and let $\mathcal{L}$ be the line
        $\mathcal{L}=\Span\Set{\vv{a}}$. }%
      \onslide<5->{Find the projection of $\vv{b}=\sage{b}$ onto $\mathcal{L}$.}
    \end{block}
    \column{.4\textwidth}
    \[
      \scalebox{.8}{%
        \begin{tikzpicture}[
          , line join=round
          , line cap=round
          , scale=1.5
          , rotate=20]
          \coordinate (O) at (0, 0);


          \pgfmathsetmacro{\mybx}{1.5}
          \pgfmathsetmacro{\myby}{1}

          \coordinate (a) at (1, 0);
          \coordinate (b) at (\mybx, \myby);
          \coordinate (p) at (\mybx, 0);

          \onslide<4->{
            \draw[ultra thick, Aquamarine!40, <->]
            ($ -0.5*(a) $) -- ($ 2*(a) $) node[below, Aquamarine] {$\mathcal{L}$};
          }

          \onslide<8->{
            \draw[ultra thick, red, ->]
            (p) -- (b) node[midway, right] {$\vv{b}-P\vv{b}$};
          }

          \onslide<2->{
            \draw[ultra thick, Aquamarine, ->]
            (O) -- (a) node[sloped, midway, below] {$\vv{a}$};
          }

          \onslide<6->{
            \draw[ultra thick, blue, ->]
            (O) -- (b) node[above] {$\vv{b}$};
          }

          \onslide<7->{
            \draw[ultra thick, violet, ->]
            (O) -- (p) node [below] {$P\vv{b}$};
          }

        \end{tikzpicture}
      }
    \]
  \end{columns}
  \onslide<9->{Note that $\mathcal{L}=\Col(A)$ where $A=\vv{a}$. }%
  \onslide<10->{The projection matrix is}
  \begin{align*}
    \onslide<11->{P
    &=} \onslide<12->{A(A^\intercal A)^{-1}A^\intercal
      =} \onslide<13->{A(\vv{a}\cdot\vv{a})^{-1}A^\intercal
      =} \onslide<14->{A(\sage{a*a})^{-1}A^\intercal \\
    &=} \onslide<15->{\sage{1/(a*a)}\sage{A}\sage{A.T}
      =} \onslide<16->{\sage{1/(a*a)}\sage{A*A.T}}
  \end{align*}
  \onslide<17->{The projection of $\vv{b}$ onto $\mathcal{L}$ is}
  \[
    \onslide<17->{P\vv{b}
      =} \onslide<18->{\sage{1/(a*a)}\sage{A*A.T}\sage{bc}
      =} \onslide<19->{\sage{1/(a*a)}\sage{A*A.T*bc}}
  \]

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    In the previous example, we had
    \begin{align*}
      A &= \sage{A} & P &= \sage{1/(a*a)}\sage{A*A.T} & \vv{b} &= \sage{bc}
    \end{align*}\pause
    We found that
    \[
      P\vv{b}
      = \sage{1/(a*a)}\sage{A*A.T}\sage{bc}
      = \sage{1/(a*a)}\sage{A*A.T*bc}
      \neq \vv{b}
    \]\pause
    Since $P\vv{b}\neq\vv{b}$, we know that $\vv{b}\notin\Col(A)$. \pause
    Additionally,
    \[
      PP\vv{b}=P^2\vv{b}=P\vv{b}
    \]\pause
    Projecting again does nothing!
  \end{block}

\end{frame}



\subsection{$\rank(A)=2$}


\begin{sagesilent}
  A = matrix([(5, -8), (2, -3), (-2, 3)])
  b = vector([0, 7, -1])
  bc = matrix.column(b)
  PZZ = A*(A.T*A).inverse()*A.T
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $A=\sage{A}$ and $\vv{b}=\sage{bc}$. \pause Note that
    \begin{align*}
      (A^\intercal A)^{-1} &= {\sage{A.T*A}}^{-1} = \sage{1/(A.T*A).det()}\sage{(A.T*A).adjoint()} \\
      P &= A(A^\intercal A)^{-1}A^\intercal = \sage{1/(A.T*A).det()}\sage{A*(A.T*A).adjoint()*A.T}
    \end{align*}\pause
    The projection of $\vv{b}$ onto $\Col(A)$ is $P\vv{b}=\sage{PZZ*b}$. \pause
    Note that $\vv{b}\notin\Col(A)$ since $P\vv{b}\neq\vv{b}$.
  \end{example}


\end{frame}


\section{Orthogonality and Projections}
\subsection{Projection onto $V^\perp$}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $P$ be the projection matrix onto $V$. The projection matrix onto
    $V^\perp$ is $Q=I-P$.
  \end{theorem}

  \pause
  \begin{block}{Sanity Check}
    Note that $Q$ is
    \begin{description}
    \item[symmetric] $Q^\intercal=\pause (I-P)^\intercal=\pause I^\intercal-P^\intercal=\pause I-P=\pause Q$\pause
    \item[idempotent] $Q^2=\pause (I-P)(I-P)=\pause I-2\,P+P^2=\pause I-P=\pause Q$
    \end{description}
  \end{block}

  \pause
  \begin{block}{Idea}
    The dimension formula states that for $V\subset\mathbb{R}^n$, we have
    \[
      \dim(V)+\dim(V^\perp)=n
    \]
    \pause Projecting onto $V^\perp$ is easier when $V$ is ``big.''
  \end{block}

\end{frame}



\subsection{Examples}

\begin{sagesilent}
  l = vector([0, 1, 1])
  lc = matrix.column(l)
  I = identity_matrix(lc.nrows())
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  From our previous example,
  \[
    \Col(A)^\perp
    = \LNull\sage{A}
    = \Span\Set{\underset{=\vv{\ell}}{\sage{l}}}
  \]\pause
  The projection matrix onto $\Col(A)^\perp$ is then\pause
  \[
    Q
    = \vv{\ell}(\vv{\ell}^\intercal\vv{\ell})^{-1}\vv{\ell}^\intercal
    = \sage{1/(l*l)}\sage{lc}\sage{lc.T}
    = \sage{1/(l*l)}\sage{lc*lc.T}
  \]\pause
  The projection matrix onto $\Col(A)$ is thus
  \[
    P
    = I-Q
    = \sage{I}-\sage{1/(l*l)}\sage{lc*lc.T}
    = \sage{1/(l*l)}\sage{l*l*I-lc*lc.T}
  \]

\end{frame}



\begin{sagesilent}
  set_random_seed(489)
  A = matrix([(1, 1, -1)])
  m, n = A.dimensions()
  r, = A.rows()
  rc = matrix.column(r)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find $P_{\Null(A)}$ where $A=\sage{A}$.
  \end{example}

  \onslide<2->
  \begin{block}{Solution}
    Note that $\dim\Null(A)=\onslide<3->\sage{A.right_nullity()}$ \onslide<4->
    while
    $\dim\Null(A)^\perp=\onslide<5->\dim\Row(A)=\onslide<6->\sage{A.rank()}$. \onslide<7->
    Projection onto $\Row(A)$ is
    \begin{align*}
      \onslide<8->{P_{\Row(A)}}
      &\onslide<9->{= \sage{rc}\pair*{\sage{rc.T}\sage{rc}}^{-1}\sage{rc.T}} \\
      &\onslide<10->{= \sage{1/(r*r)}\sage{rc*rc.T}}
    \end{align*}
    \onslide<11->{We then have $P_{\Null(A)}=I_{\sage{n}}-P_{\Row(A)}$.}
  \end{block}

\end{frame}




\end{document}
