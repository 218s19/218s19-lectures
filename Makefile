all: tex links

tex:
	find . -type f -name '*.tex' -printf '%h\n' | sort -u | parallel "cd {} && latexmk"

links:
	post-orgsite-links
