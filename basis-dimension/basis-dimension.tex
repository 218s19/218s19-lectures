\documentclass[usenames,dvipsnames]{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\PassOptionsToPackage{usenames, dvipsnames, svgnames}{xcolor}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning, quotes}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}
\usepackage{xparse}


\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Columns}{Columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Rows}{Rows}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}

\newcommand{\mysage}[2]{{#1\sage{#2}}}
\newcommand{\footsage}[1]{{\footnotesize\sage{#1}}}
\newcommand{\scriptsage}[1]{{\scriptsize\sage{#1}}}
\newcommand{\tinysage}[1]{{\tiny\sage{#1}}}




\title{Bases and Dimension of Vector Subspaces}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \tableofcontents[sections={1-3}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={4-}]
\end{frame}


\section{Definitions and Important Results}
\subsection{Definition of a Basis}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \begin{definition}
    Let $V$ be a vector subspace of $\mathbb{R}^n$. \onslide<+->A list
    $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}$ of vectors in $V$ is a
    \emph{basis} of $V$ if
    \begin{enumerate}[<+->]
    \item $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}$ is linearly independent
    \item $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}=V$
    \end{enumerate}
  \end{definition}

  \onslide<+->
  \begin{block}{Intuition}
    A basis of a vector subspace is a \emph{minimal spanning set}.
  \end{block}

\end{frame}



\begin{sagesilent}
  I = identity_matrix(3)
  e1, e2, e3 = map(matrix.column, I.columns())
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  Consider the vectors
  \begin{align*}
    \vv*{e}{1} &= \sage{e1} & \vv*{e}{2} &= \sage{e2} & \vv*{e}{3} &= \sage{e3} 
  \end{align*}
  Is $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ a basis of $\mathbb{R}^3$?

  \pause
  \begin{block}{Answer}
    Note that
    \[
      \rref
      \begin{bmatrix}
        \vv*{e}{1} & \vv*{e}{2} & \vv*{e}{3}
      \end{bmatrix}
      =\sage{I}
    \]
    \onslide<+-> Since $\rank=\#\mycolumns$,
    $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ is \onslide<+-> linearly
    independent. \onslide<+-> Since $\rank=\#\myrows$, \onslide<+->
    $\Span\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}=\mathbb{R}^3$.  \onslide<+->
    Thus $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ is a basis of $\mathbb{R}^3$.
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The list $\Set{\vv*{e}{1},\vv*{e}{2},\dotsc,\vv*{e}{n}}$ is a basis of
    $\mathbb{R}^n$.
  \end{example}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 5)
  w1, w2, w3, w4, w5 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\subsection{Important Results on Bases}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of a vector
    subspace $V$ of $\mathbb{R}^n$. If $\Set{\vv*{w}{1},\dotsc,\vv*{w}{k}}$ is a
    list of vectors in $V$ with $k>d$, then $\Set{\vv*{w}{1},\dotsc,\vv*{w}{k}}$
    is linearly dependent.
  \end{theorem}

  \pause
  \begin{example}
    Recall that $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ is a basis of
    $\mathbb{R}^3$. \pause The theorem implies that the list
    \[
      \Set*{
        \sage{w1},
        \sage{w2},
        \sage{w3},
        \sage{w4},
        \sage{w5}}
    \]
    is \pause \emph{linearly dependent} since \pause $5>3$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $V$ be a vector subspace of $\mathbb{R}^n$. Suppose that both
    \begin{align*}
      \Set{\vv*{v}{1},\dotsc,\vv*{v}{\ell}} &&\text{and}&&
                                                           \Set{\vv*{w}{1},\dotsc,\vv*{w}{m}} 
    \end{align*}
    are bases of $V$. Then $\ell=m$.
  \end{theorem}

  \pause
  \begin{block}{Note}
    This theorem says that any two bases of $V$ contain the same number of
    vectors.
  \end{block}
\end{frame}


\subsection{Definition of Dimension}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $V$ be a vector subspace of $\mathbb{R}^n$. Suppose that
    $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$. The \emph{dimension
      of $V$} is $\dim(V)=d$.
  \end{definition}

  \pause
  \begin{block}{Note}
    The dimension of a vector subspace $V$ is well-defined since any two bases
    of $V$ contain the same number of vectors.
  \end{block}

  \pause
  \begin{block}{Intuition}
    The dimension of $V$ is a measurement of how ``big'' $V$ is.
  \end{block}

  \pause
  \begin{example}
    $\dim(\mathbb{R}^n)=\pause n$ \pause because
    $\Set{\vv*{e}{1},\vv*{e}{2},\dotsc,\vv*{e}{n}}$ is a basis of $\mathbb{R}^n$
  \end{example}
\end{frame}




\begin{sagesilent}
  set_random_seed(7492)
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=3, upper_bound=4)
  v1, v2, v3 = map(matrix.column, A.columns())
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Define $V\subset\mathbb{R}^4$ by
    $V=\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ where
    \begin{align*}
      \vv*{v}{1} &= \sage{v1} & \vv*{v}{2} &= \sage{v2} & \vv*{v}{3} &= \sage{v3} 
    \end{align*}
    \pause The list $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ is linearly
    independent. \pause This list is therefore a basis of $V$\pause, so
    $\dim(V)=\pause 3$.
  \end{example}

  \pause
  \begin{block}{Note}
    In this example, we say ``$V$ is a three-dimensional vector subspace of
    $\mathbb{R}^4$.''
  \end{block}
\end{frame}


\subsection{Using Dimension to Solve Problems}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $\dim(V)=d$.
    \begin{enumerate}[<+->]
    \item If $\Span\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}=V$, then
      $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$.
    \item If $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is linearly independent, then
      $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$.
    \end{enumerate}
  \end{theorem}

  \onslide<+->
  \begin{block}{Note}
    This theorem says that checking if $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a
    basis of $V$ is easier if we know that $\dim(V)=d$.
  \end{block}
\end{frame}



\begin{sagesilent}
  set_random_seed(3)
  U = random_matrix(ZZ, 3, algorithm='unimodular')
  w1, w2, w3 = map(matrix.column, (A*U).columns())
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Previously, we defined
  $V=\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ where
  \begin{align*}
    \vv*{v}{1} &= \sage{v1} & \vv*{v}{2} &= \sage{v2} & \vv*{v}{3} &= \sage{v3} 
  \end{align*}
  \pause We found that $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ is a basis of
  $V$.  \pause It can be shown that each of
  \begin{align*}
    \vv*{w}{1} &= \sage{w1} & \vv*{w}{2} &= \sage{w2} & \vv*{w}{3} &= \sage{w3} 
  \end{align*}
  is in $V$ and that $\Set{\vv*{w}{1},\vv*{w}{2},\vv*{w}{3}}$ is linearly
  independent. \pause It follows that $\Set{\vv*{w}{1},\vv*{w}{2},\vv*{w}{3}}$
  is also basis of $V$. \pause Note that $\dim(V)=\pause 3$.

\end{frame}



\begin{sagesilent}
  set_random_seed(2930)
  A = random_matrix(ZZ, 3, algorithm='unimodular', upper_bound=9)
  B = random_matrix(ZZ, 3, algorithm='unimodular', upper_bound=9)
  C = random_matrix(ZZ, 3, algorithm='unimodular', upper_bound=9)
  D = random_matrix(ZZ, 3, algorithm='unimodular', upper_bound=9)
  a1, a2, a3 = map(matrix.column, A.columns())
  b1, b2, b3 = map(matrix.column, B.columns())
  c1, c2, c3 = map(matrix.column, C.columns())
  d1, d2, d3 = map(matrix.column, D.columns())
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Each of the lists%
    \gcenter{%
      $\Set*{\sage{a1}, \sage{a2}, \sage{a3}}$ && $\Set*{\sage{b1}, \sage{b2}, \sage{b3}}$ \\
      $\Set*{\sage{c1}, \sage{c2}, \sage{c3}}$ && $\Set*{\sage{d1}, \sage{d2}, \sage{d3}}$ 
    }%
    is linearly independent. \pause Since $\dim(\mathbb{R}^3)=3$, each of these
    lists is a basis of $\mathbb{R}^3$.
  \end{example}
\end{frame}


\section{Bases of Null Spaces}
\subsection{Null Spaces and Nullity}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Let $A$ be a $m\times n$ matrix. \pause%
    The \emph{null space} of $A$ is the vector subspace of $\mathbb{R}^n$
    defined by $\Null(A)=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}$.
  \end{block}

  \pause
  \begin{theorem}
    $\dim\Null(A)=\nullity(A)$
  \end{theorem}
\end{frame}


\subsection{Algorithm for Finding Bases of Null Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    Let $A$ be a $m\times n$ matrix. 
    \begin{itemize}
    \item<2-> The reduced row-echelon form of $A$ allows us to decompose the
      general solution to the system $A\vv{x}=\vv{O}$ as
      \[
        \vv{x}=c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_d\cdot\vv*{v}{d}
      \]
      where $\Set{c_1,c_2,\dotsc,c_d}$ are the free variables.
    \item<3-> Since $d=\nullity(A)=\dim\Null(A)$, this equation implies that
      $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}$ is a basis of $\Null(A)$.
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Examples}


\begin{sagesilent}
  A = matrix([[1,2,3,4],[1,3,5,6],[2,5,8,10]])
  var('x1 x2 x3 x4')
  var('c1 c2')
  x = matrix.column([x1, x2, x3, x4])
  xs = x(x1=c1, x2=-2*c1-2*c2, x3=c1, x4=c2)
  v1 = xs(c1=1, c2=0)
  v2 = xs(c1=0, c2=1)
\end{sagesilent}
\newcommand{\myCmatx}[1]{{#1
    \begin{bmatrix}
      c_1\\ -2\,c_1-2\,c_2\\ c_1\\ c_2
    \end{bmatrix}}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Note that
  \[
    \rref\overset{A}{\sage{A}}=\sage{A.rref()}
  \]
  \pause It follows that $\dim\Null(A)=\nullity(A)=\pause 2$. \pause
  Furthermore, the solutions to $A\vv{x}=\vv{O}$ are of the form
  \[
    \vv{x}
    = \sage{x}
    = \sage{xs}
    = c_1\overset{\vv*{v}{1}}{\sage{v1}}+c_2\overset{\vv*{v}{2}}{\sage{v2}}
  \]
  \pause This gives the basis $\Set{\vv*{v}{1}, \vv*{v}{2}}$ of $\Null(A)$.

\end{frame}


\begin{sagesilent}
  A = matrix([(-3, 9, -11, 14, 16), (2, -6, 7, -9, -10), (-5, 15, -17, 22, 24)])
  var('x1 x2 x3 x4 x5')
  var('c1 c2 c3')
  x = matrix.column([x1, x2, x3, x4, x5])
  xs = x(x1=3*c1+c2-2*c3, x2=c1, x3=c2+2*c3, x4=c2, x5=c3)
  v1 = xs(c1=1, c2=0, c3=0)
  v2 = xs(c1=0, c2=1, c3=0)
  v3 = xs(c1=0, c2=0, c3=1)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Note that
  \[
    \rref\overset{A}{\sage{A}}=\sage{A.rref()}
  \]
  \pause The solutions to $A\vv{x}=\vv{O}$ are of the form
  \[
    \vv{x}
    = \sage{x}
    = \sage{xs}
    = c_1\overset{\vv*{v}{1}}{\sage{v1}}+c_2\overset{\vv*{v}{2}}{\sage{v2}}+c_3\overset{\vv*{v}{3}}{\sage{v3}}
  \]
  \pause This gives the basis $\Set{\vv*{v}{1}, \vv*{v}{2}, \vv*{v}{3}}$ of
  $\Null(A)$.

\end{frame}


\section{Bases of Column Spaces}
\subsection{Column Spaces and Rank}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Let $A$ be a $m\times n$ matrix. The \emph{column space of $A$} is the
    vector subspace of $\mathbb{R}^m$ defined by
    $\Col(A)=\Span\Set{\textnormal{columns of }A}$.
  \end{block}

  \pause
  \begin{theorem}
    $\dim\Col(A)=\rank(A)$
  \end{theorem}
\end{frame}


\subsection{Algorithm for Finding Bases of Column Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{basic columns} of $A$ are the columns of $A$ corresponding to
    pivot columns in $\rref(A)$.
  \end{definition}

  \pause

  \begin{theorem}
    The basic columns of $A$ form a basis of $\Col(A)$.
  \end{theorem}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  A = matrix([(-8, 36, 60, 195, 36), (3, -8, -6, -40, -8), (5, -15, -15, -74, -15), (3, -13, -21, -70, -13)])
  c1, c2, c3, c4, c5 = map(matrix.column, A.columns())
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that 
    \[
      \rref\overset{A}{\sage{A}}=\sage{A.rref()}
    \]
    \pause This shows that $\dim\Col(A)=\rank(A)=\pause \sage{A.rank()}$. \pause
    Moreover,
    \[
      \Set*{\sage{c1},\sage{c2}, \sage{c4}}
    \]
    is a basis of $\Col(A)$.
  \end{example}
\end{frame}


\begin{sagesilent}
  set_random_seed(8092)
  A = random_matrix(ZZ, 3, 5, algorithm='echelonizable', rank=2, upper_bound=9)
  c1, c2, c3, c4, c5 = map(matrix.column, A.columns())
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the vector subspace $V$ of $\mathbb{R}^3$ given by
  \[
    V = \Span\Set*{\sage{c1}, \sage{c2}, \sage{c3}, \sage{c4}, \sage{c5}}
  \]
  Compute $\dim(V)$ and find a basis of $V$.
  \pause
  \begin{block}{Solution}
    Note that $V=\Col(A)$ where
    \[
      \rref\overset{A}{\sage{A}}=\sage{A.rref()}
    \]
    \pause This means that $\dim(V)=\pause\sage{A.rank()}$ and
    $\Set*{\sage{vector(c1)}, \sage{vector(c2)}}$ is a basis of $V$.
  \end{block}
\end{frame}


\section{The Rank-Nullity Theorem, Revisited}
\subsection{Rank-Nullity with Dimensions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The \emph{Rank-Nullity Theorem} states that every matrix $A$ satisfies
    \[
      \rank(A)+\nullity(A)=\#\mycolumns(A)
    \]
    Since $\rank(A)=\dim\Col(A)$ and $\nullity(A)=\dim\Null(A)$, we can restate
    this theorem.
  \end{block}

  \pause

  \begin{theorem}[The Rank-Nullity Theorem]
    Let $A$ be a $m\times n$ matrix. Then
    \[
      \dim\Col(A)+\dim\Null(A)=n
    \]
    
  \end{theorem}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  set_random_seed(398)
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=3, upper_bound=9)
  v1, v2, v3 = A.columns()
  c1, c2, c3 = map(matrix.column, A.columns())
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that $A$ is $4\times 4$ and consider the vectors
    \begin{align*}
      \vv*{v}{1} &= \sage{v1} & \vv*{v}{2} &= \sage{v2} & \vv*{v}{3} &= \sage{v3}
    \end{align*}
    Is it possible for both $\Col(A)$ and $\Null(A)$ to contain each of these
    vectors?
  \end{example}

  \pause
  \begin{block}{Solution}
    The list $\Set{\vv*{v}{1}, \vv*{v}{2}, \vv*{v}{3}}$ is linearly
    independent. \pause If both $\Col(A)$ and $\Null(A)$ contain each of these
    vectors, then $\dim\Col(A)\pause\geq 3$ \pause and
    $\dim\Null(A)\pause\geq 3$. \pause Hence
    \[
      \dim\Col(A)+\dim\Null(A)\pause\geq 3+3=6\pause>4=\#\Columns(A)
    \]
    which is impossible!
  \end{block}

\end{frame}



\subsection{Rank and Transpose}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every matrix $A$ satisfies
    \begin{align*}
      \dim\Col(A^\intercal) &= \dim\Col(A) & \dim\Row(A) &= \dim\Col(A)
    \end{align*}
    Moreover, $\dim\Col(A)=\dim\Row(A)=\rank(A)$.
  \end{theorem}
\end{frame}



\begin{sagesilent}
  A = matrix([(4, 20, -12, -7, -4), (3, 15, -9, -5, -3), (3, 15, -9, -11, -3)])
  c1, c2, c3, c4, c5 = A.columns()
  t1, t2, t3 = A.T.columns()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Note that
  \begin{align*}
    \rref\overset{A}{\sage{A}} &= \sage{A.rref()} \\
    \rref\overset{A^\intercal}{\sage{A.T}} &= \sage{A.T.rref()} 
  \end{align*}
  \pause This gives the bases
  \begin{align*}
    \Col(A) &= \Span\Set{\sage{c1}, \sage{c4}} \\
    \Row(A) &= \Span\Set{\sage{t1}, \sage{t2}}
  \end{align*}

\end{frame}


\section{Bases of Row Spaces}
\subsection{Two Algorithms for Finding Bases of Row Spaces}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Since $\Row(A)=\Col(A^\intercal)$, the basic columns of $A^\intercal$ form a
    basis of $\Row(A)$.
  \end{block}

\end{frame}

\begin{sagesilent}
  set_random_seed(2)
  A = random_matrix(ZZ, 4, 6, algorithm='echelonizable', rank=3, upper_bound=9)
  A = A.T
  v1, v2, v3, v4, v5, v6 = A.T.columns()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
      \rref\overset{A^\intercal}{\sage{A.T}}=\sage{A.T.rref()}
    \]
    \pause This gives the basis
    \[
      \Row(A) = \Span\Set{\sage{v1},\sage{v2},\sage{v4}}
    \]
    \pause Note that $\Row(A)$ is a \emph{three-dimensional subspace of }
    $\mathbb{R}^{4}$.
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\Row(A)=\Row(\rref A)$. Moreover, the nonzero rows of $\rref(A)$ form a
    basis of $\Row(A)$.
  \end{theorem}

  \pause
  \begin{block}{Note}
    Given $A$ and $\rref(A)$, this theorem allows us to find bases for both
    $\Col(A)$ and $\Row(A)$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(81)
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=3, upper_bound=9)
  r1, r2, r3, r4 = A.rref().rows()
  c1, c2, c3, c4, c5 = A.columns()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
      \rref\overset{A}{\sage{A}}=\sage{A.rref()}
    \]
    \pause This gives the bases
    \begin{align*}
      \Col(A) &= \Span\Set{\sage{c1}, \sage{c2}, \sage{c5}} \\
      \Row(A) &= \Span\Set{\sage{r1}, \sage{r2}, \sage{r3}}
    \end{align*}
  \end{example}

\end{frame}

\section{Bases of Left-Null Spaces}
\subsection{A Simple Observation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Since $\LNull(A)=\Null(A^\intercal)$, we may find a basis of $\LNull(A)$ by
    finding a basis of $\Null(A^\intercal)$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(22)
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=3, upper_bound=8)
  A = A.T
  var('x1 x2 x3 x4 x5')
  var('c1 c2')
  x = matrix.column([x1, x2, x3, x4, x5])
  xs = x(x1=3*c1-2*c2, x2=c1-3*c2, x3=c1, x4=0, x5=c2)
  xs1 = xs(c1=1, c2=0)
  xs2 = xs(c1=0, c2=1)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Note that
  \[
    \rref\overset{A^\intercal}{\sage{A.T}}
    = \sage{A.T.rref()}
  \]
  The general solution to $A^\intercal\vv{x}=\vv{O}$ is then
  \[
    \vv{x}
    = \sage{x}
    = \sage{xs}
    = c_1\overset{\vv*{v}{1}}{\sage{xs1}}+c_2\overset{\vv*{v}{2}}{\sage{xs2}}
  \]
  This gives the basis $\LNull(A)=\Span\Set{\vv*{v}{1}, \vv*{v}{2}}$.


\end{frame}

\section{Summary}
\subsection{Statements About the Four Fundamental Subspaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Given any $m\times n$ matrix $A$.
  \begin{description}[<+->][$\dim\Col(A)=\dim\Row(A)$]
  \item[$\Col(A)$ and $\LNull(A)$] are vector subspaces of $\mathbb{R}^{m}$
  \item[$\Row(A)$ and $\Null(A)$] are vector subspaces of $\mathbb{R}^{n}$
  \item[$\dim\Col(A)=\dim\Row(A)$] $=\rank(A)$
  \item[rank-nullity theorem] $\dim\Null(A)=n-\rank(A)$
  \item[rank-nullity theorem] $\dim\LNull(A)=m-\rank(A)$
  \item[basic columns of $A$] form a basis of $\Col(A)$
  \item[basic columns of $A^\intercal$] form a basis of $\Row(A)$
  \item[nonzero rows of $\rref(A)$] form a basis of $\Row(A)$
  \item[nonzero rows of $\rref(A^\intercal)$] form a basis of $\Col(A)$
  \item[basis of $\Null(A)$] obtained from $\rref(A)$
  \item[basis of $\LNull(A)$] obtained from $\rref(A^\intercal)$
  \end{description}
  
\end{frame}


\subsection{The ``Big Picture'' of Linear Algebra}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \[
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , scale=4/5
      ]

      \coordinate (O) at (0, 0);
      \coordinate (P) at (1, 2);
      \coordinate (Q) at (2, -1);
      \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
      \coordinate (Row) at ($ .5*(P)-(Q) $);
      \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

      \node at (Rn) {$\mathbb{R}^n$};

      \onslide<3->{

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{\dim=\operatorname{rank}(A)}{\operatorname{Row}(A)}$};
      }

      \onslide<4->{
        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {\scriptsize$\underset{\dim=n-\operatorname{rank}(A)}{\operatorname{Null}(A)}$};
      }



      \coordinate (v) at (-1, 2);
      \coordinate (w) at (-2, -1);
      \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
      \coordinate (Col) at ($ .5*(v)-1*(w) $);
      \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

      \tikzset{
        c/.style={every coordinate/.try}
      }

      \begin{scope}[every coordinate/.style={shift={(5,0)}}]


        \node at ([c]Rm) {$\mathbb{R}^m$};

        \onslide<5->{
          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\dim=\operatorname{rank}(A)}{\operatorname{Col}(A)}$};
        }

        \onslide<6->{
          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {\scriptsize$\underset{\dim=m-\operatorname{rank}(A)}{\operatorname{LNull}(A)}$};
        }


        \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
        (Rn) -- ([c]Rm) node[midway, above] {$A$};


        \onslide<2->{
          \path[bend right, ultra thick, shorten >=1.5em,shorten <=1.5em]
          ([c]Rm) edge["$A^\intercal$"'] (Rn);

          \path[bend right, ultra thick, ->, shorten >=1.5em,shorten <=1.5em]
          ([c]Rm) edge (Rn);
        }

      \end{scope}

    \end{tikzpicture}
  \]
  

\end{frame}

\subsection{Examples}

\begin{sagesilent}
  A = matrix([(3, 2), (-15, -10), (10, 7)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Find bases for the four fundamental subspaces of
  \[
    A = \sage{A}
  \]\pause%
  Note that
  \begin{align*}
    \rref(A) &= \sage{A.rref()} & \rref(A^\intercal) &= \sage{A.transpose().rref()}
  \end{align*}

\end{frame}

\begin{sagesilent}
  A = matrix([(-4, 5, -3, -33, 29), (-5, 6, -3, -40, 35), (1, -5, 12, 28, -27), (-1, -4, 15, 22, -23)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Find bases for the four fundamental subspaces of
  \[
    A = \sage{A}
  \]\pause%
  Note that
  \begin{align*}
    \rref(A) &= \sage{A.rref()} & \rref(A^\intercal) &= \sage{A.transpose().rref()}
  \end{align*}

\end{frame}





\end{document}
